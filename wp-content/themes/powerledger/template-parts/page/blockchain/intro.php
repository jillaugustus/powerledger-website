<?php

// Variables
$image = wp_get_attachment_image_src( get_field('blockchain_intro_image'), 'large' )[0];
$title = get_field('blockchain_intro_title');
$text = get_field('blockchain_intro_text');

if ( $image && $title && $text ) : ?>

  <section class="section section-blockchain-intro pb0 bg-device-left">
    <div class="container">

      <div class="section-column section-image-cover" style="background-image: url('<?php echo $image ?>');"></div>

      <div class="section-column section-content">
        <div class="content-wrap">
          <h2><?php echo $title ?></h2>
          <?php echo $text ?>
        </div>
      </div>

    </div>
  </section>

<?php endif;
