<?php

// Variables
$image = wp_get_attachment_image_src( get_field('values_proposition_image'), 'large' )[0];
$title = get_field('values_proposition_title');
$text = get_field('values_proposition_text');
$button = get_field('values_proposition_button');

if ( $image && $title && $text ) : ?>

  <section class="section section-values-proposition bg-device-right">
    <div class="container">

      <div class="section-column section-image-cover" style="background-image: url('<?php echo $image ?>');"></div>

      <div class="section-column section-content">
        <div class="content-wrap">
          <div class="entry-content">
            <div class="entry-content-wrap">
              <h2><?php echo $title ?></h2>
              <?php echo $text ?>
            </div>
          </div>

          <?php if( $button ) : ?>
            
            <div class="actions">
              <a href="<?php echo $button['url'] ?>" class="button"<?php if( $button['target'] ) : ?> target="blank" rel="noopener"<?php endif; ?>><?php echo $button['title'] ?></a>
            </div>

          <?php endif; ?>

        </div>
      </div>

    </div>
  </section>

<?php endif;
