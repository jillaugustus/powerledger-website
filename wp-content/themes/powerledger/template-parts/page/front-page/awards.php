<section class="section-awards bg-texture">
  <div class="container">
    <div class="awards-list">

      <article class="award award-etc">
        <div class="award-image"></div>
        <h3>WINNER</h3>
        <p>Sir Richard Branson's<br />
        Extreme Tech Challenge</p>
      </article>

      <article class="award award-fastco">
        <div class="award-image"></div>
        <h3>FINALIST</h3>
        <p>World Changing Ideas<br />
        Energy category</p>
      </article>

      <article class="award award-ey">
        <div class="award-image"></div>
        <h3>WINNER - JEMMA GREEN</h3>
        <p>Fintech Entrepreneur<br />
        of the Year</p>
      </article>

      <article class="award award-afr">
        <div class="award-image"></div>
        <h3>WINNER - JEMMA GREEN</h3>
        <p>Women of Influence<br />
        Innovation category</p>
      </article>

    </div>
  </div>
</section>