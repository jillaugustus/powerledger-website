<?php

// Variables
$image = wp_get_attachment_image_src( get_field('blockchain_why_image'), 'large' )[0];
$title = get_field('blockchain_why_title');
$text = get_field('blockchain_why_text');

if ( $image && $title && $text ) : ?>
    
  <section class="section section-blockchain-why reverse">
    <div class="container">

      <div class="section-column section-image-cover" style="background-image: url('<?php echo $image ?>');"></div>

      <div class="section-column section-content">
        <div class="content-wrap">
          <h2><?php echo $title ?></h2>
          <?php echo $text ?>
        </div>
      </div>

    </div>
  </section>

<?php endif;
