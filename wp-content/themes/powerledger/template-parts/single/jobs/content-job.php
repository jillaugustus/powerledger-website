<section class="section-post-content bg-device-left">
    <div class="entry-content">
      <div class="entry-content-wrap">
      <div class="entry-meta" aria-hidden="true">
        <h2>Position description</h2>
        <?php the_content() ?>
      </div>
      <?php get_sidebar() ?>
    </div>
  </section>