<?php 

// Set variable for page id, check first if the page is a dummy archive
if( is_archive() ) {
  $page_id = get_post_type_archive_page_id();
} else {
  $page_id = get_the_ID();
}

// Variables
$image = get_the_post_thumbnail_url( $page_id, 'full' );
$sub_title = get_field('page_subtitle', $page_id);
$button = get_field('page_header_button', $page_id);
$product_coming_soon = get_field('product_coming_soon', $page_id);

if( $image && !is_search() && !is_tax() ) : ?>
  <section class="section-generic-hero" style="background-image: url('<?php echo $image ?>');">
<?php else : ?>
  <section class="section-limited-hero">
<?php endif; ?>

  <div class="hero-content">
    
    <?php get_template_part('template-parts/entry-title'); ?>

    <?php if( is_single() && PL_PROJECTS_CPT_NAME === get_post_type() ) : ?>
      <span class="entry-sub-title h3">
        <strong><?php echo get_field('project_products'); ?></strong>
      </span>
    <?php endif; ?>

    <?php if( is_post_type_archive( PL_PROJECTS_CPT_NAME ) ) : ?>
      <span class="entry-sub-title h3">
        <?php
          $total_posts = wp_count_posts(PL_PROJECTS_CPT_NAME)->publish;
          $count = wp_count_terms('country', $args = array('hide_empty' => true));
          echo $total_posts . ' projects in ' . $count . ' countries';
        ?>
      </span>
    <?php elseif( is_single() && PL_PRODUCTS_CPT_NAME === get_post_type() && 506 === get_the_ID() && get_field('vpp_page_header_subtitle') ) : ?>
      <span class="entry-sub-title h3">
        <?php echo get_field('vpp_page_header_subtitle'); ?>
      </span>
    <?php elseif( $sub_title && !is_search() && !is_tax() ) : ?>
      <span class="entry-sub-title h3">
        <?php echo $sub_title ?>
      </span>
    <?php endif; ?>

    <?php if( is_post_type_archive( PL_PRODUCTS_CPT_NAME ) ) : ?>
      <div class="tech-buckets">
        <?php $taxonomy = get_terms( PL_PRODUCT_CATEGORIES_TAX_NAME );
        foreach( $taxonomy as $term ) : 
          if( $term->count > 0 ) : ?>
            <a href="#<?php echo $term->slug ?>" class="hero-tile tile-<?php echo $term->slug ?> smooth"><?php echo $term->name; ?></a>
          <?php endif; 
        endforeach; ?>
      </div>
    <?php endif; ?>

    <?php if( is_search() ) : ?>
      <div class="actions">
        <div class="hero-search-form">
          <?php get_search_form(); ?>
        </div>
      </div>

    <?php elseif( is_single() && PL_PRODUCTS_CPT_NAME === get_post_type() && 506 === get_the_ID() ) : ?>
      <div class="actions">
        <div class="button-group">
          <a href="#vpp-registration-form" class="button modaal-trigger">Sign up now</a>
          <?php if(get_field('button_calc_link')) : ?>
            <a href="<?php the_field('button_calc_link');?>" class="button"> <?php the_field('button_calc_text'); ?></a>
          <?php endif; ?>
        </div>
      </div>
    <?php elseif( is_single() && PL_PRODUCTS_CPT_NAME === get_post_type() && !$product_coming_soon ) : ?>
      <div class="actions">
        <a href="#enquiry-form-sales" class="button modaal-trigger">Enquire about <?php the_title(); ?></a>
      </div>
    <?php elseif( $button ) : ?>
      <div class="actions">
        <a href="<?php echo $button['url'] ?>" class="button"<?php if( $button['target'] ) : ?> target="blank" rel="noopener"<?php endif; ?>><?php echo $button['title'] ?></a>
      </div>
    <?php endif; ?>

  </div>
</section>