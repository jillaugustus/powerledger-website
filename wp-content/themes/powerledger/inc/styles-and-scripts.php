<?php

/**
 * Enqueue scripts and styles
 * 
 * @package powerledger
 */

// Enqueue front-end scripts and styles
function pl_frontend_scripts() {

  // Move WP jQuery to the footer
	// wp_dequeue_script('jquery');
	// wp_dequeue_script('jquery-core');
	// wp_dequeue_script('jquery-migrate');
	// wp_enqueue_script('jquery', false, array(), false, true);
	// wp_enqueue_script('jquery-core', false, array(), false, true);
  // wp_enqueue_script('jquery-migrate', false, array(), false, true);

  wp_deregister_script('jquery');
  wp_register_script('jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js', false, null);
  wp_enqueue_script('jquery');
  
  // Add JS files
  wp_enqueue_script( 'what-input', get_template_directory_uri() . '/js/what-input.min.js', array('jquery'), SCRIPTVERSION, true );
  wp_enqueue_script( 'slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array('jquery'), SCRIPTVERSION, true );
  wp_enqueue_script( 'modaal', 'https://cdn.jsdelivr.net/npm/modaal@0.4.4/dist/js/modaal.min.js', array('jquery'), SCRIPTVERSION, true );
  wp_enqueue_script( 'smooth-scroll', '//cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15/dist/smooth-scroll.polyfills.min.js', array('jquery'), SCRIPTVERSION, true );
  wp_enqueue_script( 'pl-functions', get_template_directory_uri() . '/js/functions.js', array('jquery'), SCRIPTVERSION, true );
  wp_enqueue_script( 'solar-demand-profile', get_template_directory_uri() . '/js/solar_demand_profile.js', array('jquery'), SCRIPTVERSION, true );
  wp_enqueue_script( 'nsw-lookup', get_template_directory_uri() . '/js/vpp-calculator/nsw_lookup.js', array('jquery'), SCRIPTVERSION, true );
  wp_enqueue_script( 'qld-lookup', get_template_directory_uri() . '/js/vpp-calculator/qld_lookup.js', array('jquery'), SCRIPTVERSION, true );
  wp_enqueue_script( 'sa-lookup', get_template_directory_uri() . '/js/vpp-calculator/sa_lookup.js', array('jquery'), SCRIPTVERSION, true );
  wp_enqueue_script( 'vic-lookup', get_template_directory_uri() . '/js/vpp-calculator/vic_lookup.js', array('jquery'), SCRIPTVERSION, true );
  wp_enqueue_script( 'vpp-calc', get_template_directory_uri() . '/js/vpp-calculator/vpp-calc.js', array('jquery'), SCRIPTVERSION, true );


  // Add CSS files
  wp_enqueue_style( 'main', get_template_directory_uri() . '/styles/style.css', array(), SCRIPTVERSION );

}

add_action( 'wp_enqueue_scripts', 'pl_frontend_scripts' );




// Enqueue admin scripts and styles
function pl_admin_scripts() {
	wp_enqueue_style( 'pl-admin', get_template_directory_uri() . '/styles/admin.css', array(), SCRIPTVERSION );
}

add_action( 'admin_enqueue_scripts', 'pl_admin_scripts' );
