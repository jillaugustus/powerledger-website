<section class="section section-full-width section-vpp-calculator-settings">
  <div class="container">

    <h1 class="section-title">Benefits calculator</h1>
    <div class="calculator-box">
      <div class="box-inner">
        <div class="box-section half">
          <label class="input-label" for="daily-usage">Daily usage
            <a href="#about-daily-usage" class="tooltip modaal-trigger">
              <span class="screen-reader-text">Learn more about daily usage</span>
            </a>
            <div id="about-daily-usage" class="modaal-content-source">
              <h2 class="h3">Daily usage</h2>
              <p>The average daily usage for a typical household in Adelaide is 18kWh. If you don't have your energy bills handy, you can estimate your energy usage based on data provided by the CSIRO.</p>
              <a href="https://ahd.csiro.au/other-data/typical-house-energy-use/ " target="_blank" rel="noopener" class="button">Learn more</a>
            </div>
          </label>
          <div class="input-options">
            <div class="input-field unit-after">
              <input id="daily-usage" name="daily-usage" type="number" value="0" min="0" max="35" pattern="\d*" />
              <span class="unit after">kWh</span>
            </div>
          </div>
        </div>
        <div class="box-section half">
          <label class="input-label" for="solar-power">Solar power</label>
          <div class="input-options">
            <div class="input-field unit-after">
              <input id="solar-power" name="solar-power" type="number" value="6" min="3" max="30" pattern="\d*" />
              <span class="unit after">kW</span>
            </div>
          </div>
        </div>
        <div class="box-section">
          <span class="input-label" >Region</span>
          <div class="input-options">
            <div class="radio-group">
              <?php pl_radio_button('region', 'region-1', 'sa', 'Adelaide, SA', true); ?>
              <?php pl_radio_button('region', 'region-2', 'nsw', 'Sydney, NSW', false); ?>
              <?php pl_radio_button('region', 'region-3', 'qld', 'Brisbane, QLD', false); ?>
              <?php pl_radio_button('region', 'region-4', 'vic', 'Melbourne, VIC', false); ?>   
            </div>
          </div>
        </div>
        <div class="box-section">
          <span class="input-label">Battery
            <a href="#about-batteries" class="tooltip modaal-trigger">
              <span class="screen-reader-text">Learn more about batteries</span>
            </a>
            <div id="about-batteries" class="modaal-content-source">
              <h2 class="h3">Battery</h2>
              <p>Power Ledger VPP 2.0 is currently limited to working with the sonnen homebatterie v8 and above.</p>
            </div>
          </span>
          <div class="input-options">
            <div class="select-option">
              <select id='batteries'>
                <option id='battery-1' name='battery' value='battery-1'>sonnen batterie hybrid 9.53/5</option>
                <option id='battery-2' name='battery' value='battery-2'>sonnen batterie hybrid 9.53/7.5</option>
                <option id='battery-3' name='battery' value='battery-3'>sonnen batterie hybrid 9.53/10</option>
                <option id='battery-4' name='battery' value='battery-4'>sonnen batterie hybrid 9.53/12.5</option>
                <option id='battery-5' name='battery' value='battery-5'>sonnen batterie hybrid 9.53/15</option>
                <option id='battery-6' name='battery' value='battery-6'>sonnen batterie eco 8.0/6</option>
                <option id='battery-7' name='battery' value='battery-7'>sonnen batterie eco 8.0/8</option>
                <option id='battery-8' name='battery' value='battery-8'>sonnen batterie eco 8.0/10</option>
                <option id='battery-9' name='battery' value='battery-9'>sonnen batterie eco 8.0/12</option>
                <option id='battery-10' name='battery' value='battery-10'>sonnen batterie eco 8.0/14</option>
                <option id='battery-11' name='battery' value='battery-11'>sonnen batterie eco 8.0/16</option>
                <option id='battery-12' name='battery' value='battery-12'>Tesla Powerwall 2</option>
                <option id='battery-13' name='battery' value='battery-13'>SENEC.Home V3 Hybrid</option>
              </select> 
            </div>
          </div>
        </div>
        <div class="box-section form-submit">
          <button id="calc-submit" class="button">Submit</button>
        </div>
      </div>
    </div>
    <div class="calculator-no-js-warning">
      <div class="message">
        <i class="icon">!</i>
        <span>JavaScript is currently disabled. Please enable it to use the calculator.</span>
      </div>
    </div>
  </div>
</section>

<section class="section section-full-width section-vpp-calculator-results">
  <div class="container">
    <div class="section-title">
      <h1>Estimated Annual Benefits</h1>
      <a href="#calculator-disclaimer" class="tooltip modaal-trigger">
        <span class="screen-reader-text">Learn more about why this date is important</span>
      </a>
      <div id="calculator-disclaimer" class="modaal-content-source">
        <h2 class="h3">Disclaimer</h2>
        <p>This calculator is intended to provide illustrative examples based on stated assumptions and your inputs.</p>
        <p>Although we've tried hard to get all the calculations correct - there is a possibility that we've made a mistake somewhere.</p>
        <p>Be aware that the default settings of the solar calculator will most likely not be applicable to your situation, and you should ensure you change all the inputs to reflect your own circumstances.</p>
        <p>Also, no one knows what the feed in tariff rates or energy prices will be in the years ahead.</p>
        <p>Calculations are meant as estimates only and are not intended to be kept or used for any practical purpose. They are based on information from sources believed to be reliable and accurate and are not intended to be used as a substitute for professional advice.</p>
        <p>The calculators are not intended to be solely relied on for the purposes of making a decision in relation to a solar power product or finance deal.</p>
        <p>Consumers should consider obtaining advice from an Australian Financial Services licensee before making any financial decision regarding mortgages, loans or solar financing.</p>
        <p>Actual outcomes will depend on a range of factors outside the control of Power Ledger.</p>
        <a class="button" href="https://homebatteryscheme.sa.gov.au/home-battery-scheme-subsidy-changes" target="_blank" rel="noopener">Learn more</a>
      </div>
    </div>
    <div class="calculator-box">
      <div class="box-inner">
        <div class="box-section">
          <h2 class="box-label h3">Total savings</h2>
            <div class="result-group">
              <div class="result-item">
                <span class="label eyebrow">Annual</span>
                <div class="result">
                  <span id="result-total" class="value total h1 big">0</span>
                  <span class="unit h4">AUD</span>
                </div>
              </div>
            </div>
        </div>
        <div class="box-section">
          <h2 class="box-label h3">Breakdown of Savings</h2>
          <div class="result-group">
            <div class="result-item ">
              <span class="label eyebrow">Self-consumption earnings</span>
              <div class="result">
                <span id="result-self-consumption" class="value h1 big">$0</span>
                <span class="unit h4">AUD</span>
              </div>
            </div>
            <div class="result-item ">
              <span class="label eyebrow">FiT earnings</span>
              <div class="result">
                <span id="result-fit" class="value h1 big">0</span>
                <span class="unit h4">AUD</span>
              </div>
            </div>
            <div class="result-item ">
              <span class="label eyebrow">VPP earnings</span>
              <div class="result">
                <span id="result-vpp" class="value h1 big">0</span>
                <span class="unit h4">AUD</span>
              </div>
            </div>
          </div>
        </div>
        <div id="battery-option-alert" class="box-section hide">
          <strong style="padding-right: 4px;">Please note:</strong><p>We do not currently support the <span id="selected-battery"></span> battery. <a href="#battery-interest-form" class="modaal-trigger">
           Click here </a>to leave your email address and be the first to know when this changes.</p>
        </div>
      </div>
    </div>
  </div>
</section>
