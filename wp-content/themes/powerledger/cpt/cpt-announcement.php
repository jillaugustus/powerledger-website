<?php

//=============================================================================================================
// Define constant variables
//=============================================================================================================

// Define constants
define( 'PL_ANNOUNCEMENTS_CPT_NAME', 'announcement' );
define( 'PL_ANNOUNCEMENTS_SINGLE_NAME', 'Announcement' );
define( 'PL_ANNOUNCEMENTS_PLURAL_NAME', 'Announcements' );

// Define permalinks
$archive_page_id = get_option( 'page_for_' . PL_ANNOUNCEMENTS_CPT_NAME );
$archive_page_slug = str_replace( home_url(), '', get_permalink( $archive_page_id ) );
$archive_permalink = ( $archive_page_id ? $archive_page_slug : 'announcement' );
$archive_permalink = ltrim( $archive_permalink, '/' );
$archive_permalink = rtrim( $archive_permalink, '/' );
define( 'PL_ANNOUNCEMENTS_REWRITE_SLUG', $archive_permalink );




//=============================================================================================================
// Register CPT
//=============================================================================================================

function pl_announcements_register_cpt() {

	$labels = array(
		'name'                      => PL_ANNOUNCEMENTS_PLURAL_NAME,
		'singular_name'             => PL_ANNOUNCEMENTS_SINGLE_NAME,
		'menu_name'                 => PL_ANNOUNCEMENTS_PLURAL_NAME,
		'name_admin_bar'            => PL_ANNOUNCEMENTS_SINGLE_NAME,
		'archives'              		=> 'Announcement archives',
		'attributes'            		=> 'Announcement attributes',
		'parent_item_colon'     		=> 'Parent announcement:',
		'all_items'             		=> 'All announcements',
		'add_new_item'          		=> 'Add new announcement',
		'add_new'               		=> 'Add new announcement',
		'new_item'              		=> 'New announcement',
		'edit_item'             		=> 'Edit announcement',
		'update_item'           		=> 'Update announcement',
		'view_item'             		=> 'View announcement',
		'view_items'            		=> 'View announcements',
		'search_items'          		=> 'Search announcement',
		'not_found'             		=> 'Not found',
		'not_found_in_trash'    		=> 'Not found in trash',
		'featured_image'        		=> 'Featured Image',
		'set_featured_image'    		=> 'Set featured image',
		'remove_featured_image' 		=> 'Remove featured image',
		'use_featured_image'    		=> 'Use as featured image',
		'insert_into_item'      		=> 'Insert into announcement',
		'uploaded_to_this_item' 		=> 'Uploaded to this announcement',
		'items_list'            		=> 'Announcements list',
		'items_list_navigation' 		=> 'Announcements list navigation',
		'filter_items_list'     		=> 'Filter announcements list'
	);

	$rewrite = array(
		'slug'                  		=> 'announcement', // The slug for single posts
		'with_front'            		=> false,
		'pages'                 		=> true,
		'feeds'                 		=> false
	);

	$args = array(
		'label'                 		=> PL_ANNOUNCEMENTS_CPT_NAME,
		'description'           		=> 'Content for a single Power Ledger announcement',
		'labels'                		=> $labels,
		'supports'              		=> array( 'title', 'editor', 'thumbnail', 'revisions', 'author' ),
		'taxonomies'            		=> array( 'article' ),
		'hierarchical'          		=> false,
		'public'                		=> true,
		'show_ui'               		=> true,
		'show_in_menu'          		=> true,
		'menu_position'         		=> 20,
		'menu_icon'             		=> 'dashicons-megaphone',
		'show_in_admin_bar'     		=> true,
		'show_in_nav_menus'     		=> true,
		'can_export'            		=> true,
		'has_archive'           		=> PL_ANNOUNCEMENTS_REWRITE_SLUG, // The slug for archive
		'exclude_from_search'   		=> false,
		'publicly_queryable'    		=> true,
		'capability_type'       		=> 'page',
		'rewrite'										=> $rewrite
	);
	register_post_type( PL_ANNOUNCEMENTS_CPT_NAME, $args );

}
add_action( 'init', 'pl_announcements_register_cpt', 0 );




// Change CPT title placeholder on edit screen
function pl_announcements_cpt_title_placeholder( $title, $post ) {

	if ( $post->post_type == PL_ANNOUNCEMENTS_CPT_NAME ) {
		return 'Add announcement title';
	}
	return $title;
}
add_filter( 'enter_title_here', 'pl_announcements_cpt_title_placeholder', 10, 2 );
