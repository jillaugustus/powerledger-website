<?php

// Variables
$product_project_filter = get_field('product_project_filter');
$requirements = get_field('requirements');

// Get projects assigned to the current product
$args = array(
  'post_type'       => PL_PROJECTS_CPT_NAME,
  'numberposts'     => 1,
  'tax_query'       => array(
    array(
      'taxonomy'      => PL_PROJECTS_PRODUCTS_TAX_NAME,
      'field'         => 'id', 
      'terms'         => $product_project_filter
    )
  )
);

// Define fallback variable
$projects_posts = NULL;

// Retrieve project posts only if the product project filter has been set
if( $product_project_filter ) :
  $projects_posts = get_posts($args);
endif;

?>

<section class="section-features<?php echo (count($projects_posts) >= 1) ? ' bg-texture' : ''; ?>">
  <div class="container">
    
    <div class="section-column section-content">
      <div class="content-wrap">
        <div class="entry-content">
          <div class="entry-content-wrap">
            <h2>Essential requirements</h2>
            <?php echo $requirements ?>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>

      