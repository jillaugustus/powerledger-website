<?php
/**
 * Template name: Contact Us
 * Template Post Type: page
 *
 * @package powerledger
 */

get_header();

// Variables
$image = get_the_post_thumbnail_url( get_the_ID(), 'full' );
$custom_title = get_field('page_custom_title');
$title = ($custom_title) ? $custom_title : get_the_title();
$sub_title = get_field('page_subtitle');
$address = get_field('address');
$email = get_field('email');
$phone = get_field('phone');

?>

<section class="section section-contact"<?php if( has_post_thumbnail() ) : ?> style="background-image: url('<?php echo $image; ?>');"<?php endif; ?>>
  
  <div class="overlay"></div>

  <div class="container">
    <div class="content-wrap">
      <h1 class="entry-title big"><?php echo $title ?></h1>
      <span class="entry-sub-title h3"><?php echo $sub_title ?></span>

      <div class="contact-forms">
        <div class="button-group">
          <a class="button modaal-trigger" href="#enquiry-form-general">General enquiry</a>
          <a class="button modaal-trigger" href="#enquiry-form-sales">Sales enquiry</a>
          <a class="button modaal-trigger" href="#enquiry-form-media">Media enquiry</a>
        </div>
      </div>

      <div class="contact-details">
        
        <address>
          <strong>Headquarters</strong>
          <?php echo $address ?>
        </address>

        <a class="inline-link" href="mailto:<?php echo $email ?>"><?php echo $email ?></a>

        <span><?php echo $phone ?></span>

      </div>
    </div>
  </div>
</section>

<?php 
get_footer();
