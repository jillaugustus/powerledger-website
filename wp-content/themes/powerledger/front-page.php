<?php
/**
 * The template for displaying the front page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package powerledger
 */

get_header(); ?>

<main class="front-page">

  <?php

  get_template_part('template-parts/page/front-page/hero');

  get_template_part('template-parts/page/front-page/about');

  get_template_part('template-parts/project-carousel');

  get_template_part('template-parts/page/front-page/products');

  get_template_part('template-parts/page/front-page/awards');

  get_template_part('template-parts/global-enquiry');

  ?>

</main>

<?php
get_footer();
