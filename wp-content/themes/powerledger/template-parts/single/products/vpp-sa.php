<?php
/**
 * Template name: VPP Registration - SA Specific
 * Template Post Type: page
 * 
 * @package powerledger
 */

get_header();

?>

<section class="section-product-overview reverse pb0">
  <div class="container">

    <div class="section-column section-video">
      <?php the_field('what_is_it_media'); ?>
    </div>
    
    <div class="section-column section-content">
      <div class="content-wrap">
        <h2>What is a VPP?</h2>
        <?php the_field('what_is_it_text'); ?>
      </div>
    </div>
  
  </div>
</section>

<section class="section-product-overview pb0">
  <div class="container">
    
    <div class="section-column section-video">
      <?php the_field('how_does_it_work_media'); ?>
    </div>

    <div class="section-column section-content">
      <div class="content-wrap">
        <h2>How does the VPP work?</h2>
        <?php the_field('how_does_it_work_text'); ?>
      </div>
    </div>
    
  </div>
</section>

<section class="section-product-benefits">
  <div class="container">

    <div class="benefit">
      <p><?php the_field('benefit_1'); ?></p>
    </div>

    <div class="benefit">
      <p><?php the_field('benefit_2'); ?></p>
    </div>

  </div>
</section>

<section class="section section-features bg-texture">
  <div class="container">

    <div class="section-column section-content">
      <div class="content-wrap">
        <div class="entry-content">
          <div class="entry-content-wrap">
            <h2>Highlights</h2>
            <?php the_field('highlights'); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="section-column section-content">
      <div class="content-wrap">
        <div class="entry-content">
          <div class="entry-content-wrap">
            <h2>Essential requirements</h2>
            <?php the_field('requirements'); ?>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</section>

<section class="section-product-steps bg-device-right">
  <div class="container">
    <h2>Getting started</h2>
    <div class="getting-started-steps">
      <article style="background-image: url('<?php echo wp_get_attachment_image_src( get_field('step_1_image'), "article" )[0]; ?>');">
        <h3>Step 1</h3>
        <p><?php the_field('step_1'); ?></p>
      </article>
      <article style="background-image: url('<?php echo wp_get_attachment_image_src( get_field('step_2_image'), "article" )[0]; ?>');">
        <h3>Step 2</h3>
        <p><?php the_field('step_2'); ?></p>
      </article>
      <article style="background-image: url('<?php echo wp_get_attachment_image_src( get_field('step_3_image'), "article" )[0]; ?>');">
        <h3>Step 3</h3>
        <p><?php the_field('step_3'); ?></p>
      </article>
      <article style="background-image: url('<?php echo wp_get_attachment_image_src( get_field('step_4_image'), "article" )[0]; ?>');">
        <h3>Step 4</h3>
        <p><?php the_field('step_4'); ?></p>
      </article>
    </div>
  </div>
</section>

<section class="section-enquiry bg-texture">
  <div class="container">
    <h2>Register your interest</h2>
    <?php the_field('call_to_action'); ?>
    <a href="#vpp-registration-form" class="button modaal-trigger">Sign up now</a>
    <?php if(get_field('button_calc_link')) : ?>
      <a href="<?php the_field('button_calc_link');?>" class="button"> <?php the_field('button_calc_text'); ?></a>
    <?php endif; ?>
  </div>
</section>

<?php 
get_footer();
