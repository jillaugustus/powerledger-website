<?php 

// Set empty array to store each value
$highlights_array = array();

// Set the max number of value items possible
$highlight_count = 6;

// Increment and loop through the count above and conditionally add to array if ACF fields are set
for($i = 1; $i<=$highlight_count; $i++) {

  if( get_field('values_value_' . $i) ) {
    $highlights_array[] = get_field('values_value_' . $i);
  }
}

// Check if the array is not still empty before displaying the section
if( !empty( $highlights_array ) ) : ?>

  <section class="section section-tiles grid">
    <div class="container">
      
      <div class="section-header">
        <h2 class="section-title">Our values</h2>
      </div>

      <div class="tile-grid">

        <?php foreach ( $highlights_array as $key => $value ) : ?>

          <p class="grid-item"><?php echo $value ?></p>

        <?php endforeach; ?>

      </div>
    </div>
  </section>

<?php endif;
