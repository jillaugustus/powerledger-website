<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package powerledger
 */

get_header();

get_template_part('template-parts/page-header');

?>

<main class="search-results-page">
  <section class="section section-search-results">
    <div class="container">

      <?php if ( have_posts() ) : ?>

        <div class="pages-list">

          <?php while ( have_posts() ) : the_post(); 

            get_template_part( 'template-parts/content', 'search' );

          endwhile; ?>

        </div>

      <?php else :

        get_template_part( 'template-parts/content', 'none' );

      endif;
    
      get_template_part( 'template-parts/pagination' ); ?>

    </div>
  </section>
</main>

<?php
get_footer();
