<?php 

// Variables
$image = wp_get_attachment_image_src( get_field('how_does_it_work_image'), 'large' )[0];
$video = get_field('how_does_it_work_video');
$title = get_field('how_does_it_work_title');
$text = get_field('how_does_it_work_text');
$button = get_field('how_does_it_work_button');

if( $text & $title ) : ?>

  <section class="section section-product-how">
    <div class="container">

      <?php if( $video ) : ?>

        <div class="section-column section-video">
          <?php echo $video ?>
        </div>

      <?php elseif( $image ) : ?>

        <div class="section-column section-image-cover" style="background-image: url('<?php echo $image; ?>');"></div>

      <?php endif; ?>

      <div class="section-column section-content">
        <div class="content-wrap">

          <h2><?php echo $title ?></h2>
          <?php echo $text ?>
          
          <?php if( $button ) : ?>
              
            <div class="actions">
              <a href="<?php echo $button['url'] ?>" class="button"<?php if( $button['target'] ) : ?> target="blank" rel="noopener"<?php endif; ?>><?php echo $button['title'] ?></a>
            </div>

          <?php endif; ?>

        </div>
      </div>

    </div>
  </section>

<?php endif;
