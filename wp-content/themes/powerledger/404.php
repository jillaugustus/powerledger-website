<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package powerledger
 */

get_header();
?>

<section class="section section-error-404 not-found">
  <div class="container">
    <div class="content-wrap">
      <h1 class="entry-title big">Sorry...</h1>
      <span class="entry-sub-title h3">The page you're looking for doesn't exist.</span>
      <div class="actions">
        <a href="<?php echo home_url(); ?>" class="button">Return home</a>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
