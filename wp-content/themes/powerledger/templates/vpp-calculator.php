<?php
/**
 * Template name: VPP Calculator
 * Template Post Type: page
 * 
 * @package powerledger
 */

get_header(); 
  
  get_template_part('template-parts/page-header');

  get_template_part('template-parts/page/vpp-calculator/intro');

  get_template_part('template-parts/page/vpp-calculator/savings-timeline');

  get_template_part('template-parts/page/vpp-calculator/calculator');

  get_template_part('template-parts/page/vpp-calculator/sign-up');

get_footer();
