<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package powerledger
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="alternate" type="application/rss+xml" title="News feed" href="/feed.xml">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="theme-color" content="#ffffff">

  <meta name="Description" content="We help people transact energy and trade environmental commodities.">
  <meta property="og:description" content="We help people transact energy and trade environmental commodities.">
  <meta name="twitter:description" content="We help people transact energy and trade environmental commodities.">

  <?php

  // Set variable for page id, check first if the page is a dummy archive
  if( is_archive() ) {
    $page_id = get_post_type_archive_page_id();
  } else {
    $page_id = get_the_ID();
  }

  $image = get_the_post_thumbnail_url( $page_id, 'large' );

  if( $image ) {

    // Yoast adds og:image automatically if a featured image is set for pages and posts, but wont recognise our dummy archive pages so we need to set it here
    if( is_archive() ) {
      echo '<meta property="og:image" content="' . $image . '">';
    }

    echo '<meta name="twitter:image" content="' . $image . '">';
  }
  else {
    echo '<meta property="og:image" content="' . get_template_directory_uri() . '/images/pl-logo.jpg">';
    echo '<meta name="twitter:image" content="' . get_template_directory_uri() . '/images/pl-logo.jpg">';
  }
  
  // Only add tracking and analytics code for production environment
  if( APPLICATION_ENV === 'production' ) : ?>
    <meta name="google-site-verification" content="Gk0uBZqOyGhut9GjxXEZS9h7C_Jt8JgEGwEaICVA7QI" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79545836-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-79545836-1');
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5RT82TP');</script>
    <!-- End Google Tag Manager -->

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '408147649835559');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=408147649835559&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Hotjar Tracking Code for www.powerledger.io -->
    <script>
      (function(h,o,t,j,a,r){
          h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
          h._hjSettings={hjid:1721062,hjsv:6};
          a=o.getElementsByTagName('head')[0];
          r=o.createElement('script');r.async=1;
          r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
          a.appendChild(r);
      })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
  
  <?php endif;

  wp_head(); ?>

</head>

<body <?php body_class('no-js'); ?>>

  <script>document.body.className = document.body.className.replace("no-js","js");</script>

  <?php 
  // Only add tracking and analytics code for production environment
  if( APPLICATION_ENV === 'production' ) : ?>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RT82TP"
    height="0" width="0" style="display:none;visibility:hidden" aria-label="Google tag manager"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

  <?php endif; ?>

  <header class="header">

    <div class="header-bar">
      <div class="header-bar-wrap">

        <div class="header-left">
          <a href="<?php echo get_home_url(); ?>" class="header-logo">
            <img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logo-powerledger.svg" alt="<?php bloginfo( 'name' ); ?>" />
          </a>
        </div>

        <div class="header-right">

          <nav class="desktop-menu">
            <?php
              wp_nav_menu( array(
                'container'         => false,
                'theme_location'    => 'main-menu',
                'menu_class'        => 'main-menu',
                'fallback_cb'       => false,
                'depth'             => 1
              ));
            ?>
          </nav>

          <div class="site-tools">
            <div class="translate-trigger">
              <?php echo do_shortcode('[gtranslate]'); ?>
            </div>

            <button class="search-panel-trigger" title="Search">
              <span class="screen-reader-text">Search</span>
              <div class="label-swapper zoom">
                <div class="label">
                    <i class="search-icon"></i>
                </div>
                <div class="label">
                    <i class="close-icon"></i>
                </div>
              </div>
            </button>

            <button class="mobile-menu-trigger" title="Mobile Menu">
              <span class="screen-reader-text">Mobile Menu</span>
              <div class="inner">
                <div class="hamburger">
                  <div></div>
                  <div></div>
                  <div></div>
                </div>
              </div>
            </button>
          </div>

        </div>

      </div>
    </div>

    <div class="overlay-panel">
      <div class="container">
        <div class="panel-wrap">
          <div class="panel-scroll">

            <div class="mobile-menu">
              <nav class="mobile-nav" aria-label="Mobile Menu">
                <?php
                  wp_nav_menu( array(
                    'container'         => false,
                    'theme_location'    => 'main-menu',
                    'menu_class'        => 'main-menu',
                    'fallback_cb'       => false,
                    'depth'             => 1
                  ));
                ?>
              </nav>
            </div>

            <div class="search-panel">
              <?php get_search_form(); ?>
            </div>

          </div>
        </div>
      </div>
    </div>

  </header>

  <div class="page-wrapper">