<?php

// Variables
$archive_page_id = get_post_type_archive_page_id();

$image = wp_get_attachment_image_src( get_field('jobs_archive_intro_image', $archive_page_id), 'large' )[0];
$title = get_field('jobs_archive_intro_title', $archive_page_id);
$text = get_field('jobs_archive_intro_text', $archive_page_id);
$button = get_field('jobs_archive_intro_button', $archive_page_id);

if ( $image && $title && $text ) : ?>

  <section class="section section-values-intro bg-device-left reverse">
    <div class="container">

      <div class="section-column section-image-cover" style="background-image: url('<?php echo $image ?>');"></div>

      <div class="section-column section-content">
        <div class="content-wrap">
          <div class="entry-content">
            <div class="entry-content-wrap">
              <h2><?php echo $title ?></h2>
              <?php echo $text ?>
            </div>
          </div>

          <?php if( $button ) : ?>
            
            <div class="actions">
              <a href="<?php echo $button['url'] ?>" class="button"<?php if( $button['target'] ) : ?> target="blank" rel="noopener"<?php endif; ?>><?php echo $button['title'] ?></a>
            </div>

          <?php endif; ?>

        </div>
      </div>

    </div>
  </section>

<?php endif;
