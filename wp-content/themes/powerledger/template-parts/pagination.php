<?php if( $wp_query->max_num_pages > 1 ) : ?>

	<div class="posts-navigation">
		<nav class="pagination" aria-label="Page Navigation">

			<?php	echo pl_pagination();	?>
				
		</nav>
	</div>
    
<?php endif;
