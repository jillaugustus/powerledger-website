<?php 

// Variables
$title = get_field('vpp_calculator_register_title');
$text = get_field('vpp_calculator_register_text');

?>

<section class="section-enquiry bg-texture">
  <div class="container">
    <div class="content">
      <h2><?php if($title) : echo $title; else : echo 'Ready to join?'; endif; ?></h2>
      <?php if($text) : 
        echo $text;
      endif; ?>
      <a href="#vpp-registration-form" class="button modaal-trigger">Sign up for VPP</a>
    </div>
  </div>
</section>
