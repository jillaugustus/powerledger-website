<?php

// Variables
$product_project_filter = get_field('product_project_filter');

// Setup query arguments
$args = array(
  'post_type'       => PL_PROJECTS_CPT_NAME,
  'orderby'         => 'menu_order',
  'order'           => 'ASC',
);

// If project list is being filtered by the product taxonomy, then add a tax query to the query arguments
if( $product_project_filter ) {
  $args['tax_query'] = array(
    array (
      'taxonomy'      => PL_PROJECTS_PRODUCTS_TAX_NAME,
      'field'         => 'id',
      'terms'         => $product_project_filter,
    )
  );
}

$projects_loop = new WP_Query($args);

if( $projects_loop -> have_posts() ): ?>

  <section class="section-projects section-carousel<?php echo ( is_front_page() ) ? ' bg-texture' : ''; ?>">

    <div class="container">
      <div class="section-header">
      
        <h2 class="section-title h1">
          <?php echo ( PL_PRODUCTS_CPT_NAME === get_post_type() ) ? 'Projects using ' . get_the_title() : 'Our projects'; ?>
        </h2>

        <?php if ( is_front_page() ) : ?>

          <span class="section-sub-title h3">
            <?php
              $total_posts =  wp_count_posts('project')->publish;
              $count = wp_count_terms('country', $args = array('hide_empty' => true));
              echo $total_posts . ' projects in ' . $count . ' countries';
            ?>
          </span>

         <?php endif; ?>
       </div>
    </div>

    <div class="carousel-wrapper">

      <?php while( $projects_loop -> have_posts() ) : $projects_loop -> the_post(); 

        // Variables
        $image = get_the_post_thumbnail_url( get_the_ID(), 'full' );
        $custom_title = get_field('page_custom_title');
        $sub_title = get_field('page_subtitle');

        ?>

        <a href="<?php the_permalink(); ?>" aria-label="<?php echo $custom_title ?>">
          <article>
            <div class="article-image" style="background-image: url('<?php echo $image ?>');"></div>
            <h3><?php echo $custom_title ?></h3>
            <p aria-hidden="true"><?php echo $sub_title ?></p>
            <span>Read more</span>
          </article>
        </a>

      <?php endwhile; wp_reset_postdata(); ?>

    </div>

  </section>  

<?php endif;
