<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package powerledger
 */

get_header();

  get_template_part('template-parts/page-header'); ?>

  <main class="full-width-page">
    <div class="container">

      <?php while ( have_posts() ) : the_post();
        get_template_part( 'template-parts/content', 'page' );
      endwhile; ?>

    </div>
  </main>

  <?php
  get_template_part('template-parts/global-enquiry');

get_footer();
