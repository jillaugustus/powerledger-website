<?php 

/**
 * Pagination navigation
 */
function pl_pagination() {

	$settings = array(
		'count' => 5,
		'prev_text' => '<i class="icon"></i><span class="screen-reader-text">Previous page</span>',
		'next_text' => '<i class="icon"></i><span class="screen-reader-text">Next page</span>'
	);

	global $wp_query;
	$current = max( 1, get_query_var( 'paged' ) );
	$total = $wp_query->max_num_pages;
	$links = array();

	// Offset for next link
	if( $current < $total )
		$settings['count']--;

	// Previous
	if( $current > 1 ) {
		$settings['count']--;
		$links[] = pl_pagination_link( $current - 1, 'prev', $settings['prev_text'] );
	}

	// Current
	$links[] = pl_pagination_link( $current, 'current' );

	// Next Pages
	for( $i = 1; $i < $settings['count']; $i++ ) {
		$page = $current + $i;
		if( $page <= $total ) {
			$links[] = pl_pagination_link( $page );
		}
	}

	// Next
	if( $current < $total ) {
		$links[] = pl_pagination_link( $current + 1, 'next', $settings['next_text'] );
	}

	// Output
	echo join( '', $links );
}




/**
 * Pagination navigation link
 *
 * @param int $page
 * @param string $class
 * @param string $label
 * @return string $link
 */
function pl_pagination_link( $page = false, $class = '', $label = '' ) {

	if( ! $page )
		return;

	$classes = array( 'page-numbers' );
	if( !empty( $class ) )
		$classes[] = $class;
	$classes = array_map( 'sanitize_html_class', $classes );

	$label = $label ? $label : $page;
	$link = esc_url_raw( get_pagenum_link( $page ) );

	return '<a class="' . join ( ' ', $classes ) . '" href="' . $link . '">' . $label . '</a>';

}
