<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package powerledger
 */

?>

<?php if( !is_single() ) : ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-wrapper">
			<a class="entry-link" href="<?php the_permalink(); ?>" target="_self">
				<div class="entry-inner">

					<div class="entry-thumb"<?php if( has_post_thumbnail() ) : ?> style="background-image: url('<?php echo get_the_post_thumbnail_url( get_the_ID(), 'medium' ); ?>');"<?php endif; ?>>
						<div class="ratio-sizer"></div>
					</div>
					
					<div class="entry-preview">

						<div class="color-curtain"></div>

						<div class="entry-preview-wrap">
							<div class="entry-preview-content">
								<h2 class="entry-title h3"><?php echo the_title(); ?></h2>

								<div class="entry-meta" aria-hidden="true">
									<span class="posted-on"><?php echo get_the_date(); ?></span><span class="categories"><?php echo strip_tags ( get_the_term_list( get_the_ID(), 'category', "",", " ) ); ?></span>
								</div>
							</div>
						</div>

					</div>
					
				</div>
			</a>
		</div>
	</article>

<?php else : ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="entry-content">
			<div class="entry-content-wrap">

				<?php
				
					the_content();
				
				?>

			</div>

		</div>

	</article>

<?php endif;