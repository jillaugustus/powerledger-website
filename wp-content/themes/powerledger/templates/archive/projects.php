<?php

// Variables
$archive_page_id = get_post_type_archive_page_id();
$list_title = get_field('projects_archive_list_title', $archive_page_id);

get_template_part('template-parts/archive/projects/intro');

if( have_posts() ) : ?>

  <section class="section section-projects bg-device-left">
    <div class="container">
      
      <div class="section-header">
        <h2 class="section-title h1"><?php echo ($list_title) ? $list_title : 'Our projects'; ?></h2>
      </div>

      <div class="projects-list">

        <?php while( have_posts() ) : the_post(); 
          
          // Variables
          $image = get_the_post_thumbnail_url( get_the_ID(), 'full' );
          $custom_title = get_field('page_custom_title');
          $sub_title = get_field('page_subtitle');
          
          ?>

          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <a class="entry-link" href="<?php the_permalink(); ?>" target="_self"<?php if($image) : ?> style="background-image: url('<?php echo $image ?>');"<?php endif; ?>>
              <div class="overlay"></div>
              <div class="entry-preview">
                <div class="entry-meta">
                  <?php
                    $terms = get_the_terms( get_the_ID(), PL_PROJECTS_PRODUCTS_TAX_NAME );
                    foreach ($terms as $term) {
                      echo '<span>' . $term->name . '</span>';
                    }
                  ?>
                </div>
                <h2 class="entry-title h4"><?php echo ($custom_title) ? $custom_title : the_title(); ?></h2>
              </div>
            </a>
          </article>
        <?php endwhile; ?>
      </div>
    </div>
  </section>

<?php endif;
