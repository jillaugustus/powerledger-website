<?php

// Variables
$archive_page_id = get_post_type_archive_page_id();
$list_title = get_field('jobs_archive_list_title', $archive_page_id);
$list_sub_title = get_field('jobs_archive_list_sub_title', $archive_page_id);

get_template_part('template-parts/archive/jobs/intro');

?>

<section class="section section-positions pt0">
  <div class="container">

    <?php if( have_posts() ) : ?>

      <div class="section-header">
        <h2 class="section-title"><?php echo ($list_title) ? $list_title : 'Open positions'; ?></h2>
        <?php if($list_sub_title) : ?>
          <span class="section-sub-title"><?php echo $list_sub_title ?></span>
        <?php endif; ?>
      </div>

      <div class="jobs-list">

        <?php while( have_posts() ) : the_post();

          // Variables
          $custom_title = get_field('page_custom_title');
          $sub_title = get_field('page_subtitle');
        
          ?>

          <div class="job-item">
            <a href="<?php the_permalink(); ?>" aria-label="<?php the_field('title'); ?>" class="article-link">
              <h2 class="title h3"><?php echo ($custom_title) ? $custom_title : the_title(); ?></h2>
              <?php if( $sub_title ) : ?>
                <p class="sub-title"><?php echo $sub_title ?></p>
              <?php endif; ?>
            </a>
          </div>
        <?php endwhile; ?>

      </div>

    <?php else: ?>
      <div class="section-header">
        <h2 class="section-title"><?php echo ($list_title) ? $list_title : 'Open positions'; ?></h2>
        <span class="section-sub-title">There are no current positions, please check back soon.</span>
      </div>
    <?php endif; ?>

  </div>
</section>
