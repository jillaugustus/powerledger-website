<?php 

// Variables
$image = wp_get_attachment_image_src( get_field('product_coming_soon_image'), 'large' )[0];
$title = get_field('product_coming_soon_title');
$text = get_field('product_coming_soon_text');

if ( $image && $title && $text ) : ?>

  <section class="section-about-intro bg-device-left reverse">
    <div class="container">
      
      <div class="section-column section-image-cover" style="background-image: url('<?php echo $image ?>');"></div>

      <div class="section-column section-content">
        <div class="content-wrap">
          <h2><?php echo $title ?></h2>
          <?php echo $text ?>
        </div>
      </div>

    </div>
  </section>

<?php endif; 
