<?php 

// Variables
$text = get_field('blockchain_quote_2_text');
$author = get_field('blockchain_quote_2_author');

if( $text && $author ) : ?>

  <section class="section section-testimonial bg-texture">
    <div class="container">
      <h3 class="quote big"><?php echo $text ?></h3>
      <strong class="attribution"><?php echo $author ?></strong>
    </div>
  </section>

<?php endif;
