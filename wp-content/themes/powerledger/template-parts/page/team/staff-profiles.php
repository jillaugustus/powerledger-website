<?php
  $args = array(
    'post_type'         => PL_STAFF_CPT_NAME,
    'orderby'           => 'menu_order',
    'order'             => 'ASC',
    'posts_per_page'    => -1
  );
  $staff = new WP_Query($args);
?>


<?php if( $staff -> have_posts() ): ?>

  <section class="section section-team bg-device-left">
    <div class="container">
      
      <?php while( $staff -> have_posts() ) : $staff -> the_post(); 
      
        $image = get_the_post_thumbnail_url( get_the_ID(), 'full' );
        $position = get_field('title');
        $description = get_field('description');
        
        ?>
        
        <article>
          <div class="profile-image" style="background-image: url('<?php echo $image ?>');"></div>
          <div class="profile-content">
            <h3 class="name"><?php the_title(); ?></h3>
            <span class="position"><?php echo $position ?></span>
            <p class="description"><?php echo $description ?></p>
          </div>
        </article>

      <?php endwhile; wp_reset_postdata(); ?>

    </div>
  </section>

<?php endif; ?>