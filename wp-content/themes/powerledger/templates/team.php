<?php
/**
 * Template name: Team
 * Template Post Type: page
 * 
 * @package powerledger
 */

get_header();
  
get_template_part('template-parts/page-header');

get_template_part('template-parts/page/team/staff-profiles');

get_template_part('template-parts/global-enquiry');

get_footer();
