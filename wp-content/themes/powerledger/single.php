<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package powerledger
 */

// Modifier class
if( PL_PROJECTS_CPT_NAME === get_post_type() || PL_PRODUCTS_CPT_NAME === get_post_type() ) :
	$page_class = '';
else :
	$page_class = ' standard-page';
endif;

get_header();

get_template_part('template-parts/page-header'); ?>

	<main class="<?php echo 'single-' . strtolower( get_post_type_object( get_post_type() )->labels->singular_name ); echo $page_class; ?>">

		<?php
		
		while ( have_posts() ) : the_post();

			if( PL_PROJECTS_CPT_NAME === get_post_type() ) : 

				get_template_part('templates/single/project');

			elseif( PL_PRODUCTS_CPT_NAME === get_post_type() ) : 

				get_template_part('templates/single/product');

			elseif( PL_JOBS_CPT_NAME === get_post_type() ) : 

				get_template_part('template-parts/single/jobs/content-job');
				
			elseif( PL_ARTICLES_CPT_NAME === get_post_type() || PL_ANNOUNCEMENTS_CPT_NAME === get_post_type() ) :

				get_template_part('template-parts/content', 'post');
			
			else : ?>

				<div class="container">

					<?php get_template_part( 'template-parts/content', get_post_type() ); ?>

				</div>

			<?php endif;

		endwhile;
		
		?>

	</main>

<?php
get_footer();
