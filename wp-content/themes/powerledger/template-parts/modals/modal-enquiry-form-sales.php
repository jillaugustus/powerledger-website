<div id="enquiry-form-sales" class="modaal-content-source">
  <h2 class="h3">Sales enquiry</h2>
  <!-- Embedding hubspot contact form -->
  <!--[if lte IE 8]>
  <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
  <![endif]-->
  <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
  <script>
    hbspt.forms.create({
      portalId: "4519667",
      formId: "7dea0d8d-465f-4000-a6c7-4e960dc7bbb8"
    });
  </script>
</div>
