<?php

//=============================================================================================================
// Define constant variables
//=============================================================================================================

// Define constants
define( 'PL_STAFF_CPT_NAME', 'staff' );
define( 'PL_STAFF_SINGLE_NAME', 'Staff Member' );
define( 'PL_STAFF_PLURAL_NAME', 'Staff Members' );

// Define permalinks
define( 'PL_STAFF_REWRITE_SLUG', 'staff' );




//=============================================================================================================
// Register CPT
//=============================================================================================================

function pl_staff_register_cpt() {

	$labels = array(
		'name'                      => PL_STAFF_PLURAL_NAME,
		'singular_name'             => PL_STAFF_SINGLE_NAME,
		'menu_name'                 => PL_STAFF_PLURAL_NAME,
		'name_admin_bar'            => PL_STAFF_SINGLE_NAME,
		'archives'              		=> 'Staff Archives',
		'attributes'            		=> 'Staff Attributes',
		'parent_item_colon'     		=> 'Parent Staff Member:',
		'all_items'             		=> 'All Staff Members',
		'add_new_item'          		=> 'Add New Staff Member',
		'add_new'               		=> 'Add New Staff Member',
		'new_item'              		=> 'New Staff Member',
		'edit_item'             		=> 'Edit Staff Member',
		'update_item'           		=> 'Update Staff Member',
		'view_item'             		=> 'View Staff Member',
		'view_items'            		=> 'View Staff Members',
		'search_items'          		=> 'Search Staff Members',
		'not_found'             		=> 'Not found',
		'not_found_in_trash'    		=> 'Not found in Trash',
		'featured_image'        		=> 'Profile Image',
		'set_featured_image'    		=> 'Set profile image',
		'remove_featured_image' 		=> 'Remove profile image',
		'use_featured_image'    		=> 'Use as profile image',
		'insert_into_item'      		=> 'Insert into item',
		'uploaded_to_this_item' 		=> 'Uploaded to this item',
		'items_list'            		=> 'Staff list',
		'items_list_navigation' 		=> 'Staff list navigation',
		'filter_items_list'     		=> 'Filter staff list',
	);

	$rewrite = array(
		'slug'                  		=> PL_STAFF_REWRITE_SLUG,
		'with_front'            		=> false,
		'pages'                 		=> true,
		'feeds'                 		=> false
	);

	$args = array(
		'label'                 		=> PL_STAFF_SINGLE_NAME,
		'description'           		=> 'Content for a single Power Ledger staff member',
		'labels'                		=> $labels,
		'supports'              		=> array( 'title', 'thumbnail', 'revisions', 'page-attributes' ),
		'taxonomies'            		=> array(),
		'hierarchical'          		=> false,
		'public'                		=> true,
		'show_ui'               		=> true,
		'show_in_menu'          		=> true,
		'menu_position'         		=> 20,
		'menu_icon'             		=> 'dashicons-admin-users',
		'show_in_admin_bar'     		=> true,
		'show_in_nav_menus'     		=> true,
		'can_export'            		=> true,
		'has_archive'           		=> false,
		'exclude_from_search'   		=> false,
		'publicly_queryable'    		=> false,
		'capability_type'       		=> 'page',
		'rewrite'                   => $rewrite
	);
	register_post_type( PL_STAFF_CPT_NAME, $args );

}
add_action( 'init', 'pl_staff_register_cpt', 0 );




// Change CPT title placeholder on edit screen
function pl_staff_cpt_title_placeholder( $title, $post ) {

	if ( $post->post_type == PL_STAFF_CPT_NAME ) {
		return 'Add staff member name';
	}
	return $title;
}
add_filter( 'enter_title_here', 'pl_staff_cpt_title_placeholder', 10, 2 );




// Register a new column in admin list view for menu order
function pl_staff_cpt_admin_table_column( $defaults ) {
	
	$new_order = array();

	foreach( $defaults as $key=>$value ) {
		if( $key=='date' ) {  // When we find the date column
			$new_order['menu_order'] = 'Order'; // Slip in the new column before it
		}
		$new_order[$key] = $value;
	}

	return $new_order;
}
add_filter( 'manage_' . PL_STAFF_CPT_NAME . '_posts_columns', 'pl_staff_cpt_admin_table_column' );




// Return the menu order in admin list view for each post
function pl_staff_cpt_admin_table_content( $column_name, $post_id ) {
		
	global $post;
		
	if ( $column_name == 'menu_order' ) {
		$order = $post->menu_order;
		echo $order;
	}
}
add_action( 'manage_' . PL_STAFF_CPT_NAME . '_posts_custom_column', 'pl_staff_cpt_admin_table_content', 10, 2 );




// Make the admin menu order column sortable
function pl_staff_cpt_admin_table_sortable( $columns ) {
	$columns['menu_order'] = 'menu_order';
	return $columns;
}
add_filter( 'manage_edit-' . PL_STAFF_CPT_NAME . '_sortable_columns', 'pl_staff_cpt_admin_table_sortable' );
