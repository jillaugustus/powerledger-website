<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package powerledger
 */

?>

<div class="no-results not-found">
  <div class="no-results-message">

		<?php if( is_post_type_archive( PL_PROJECTS_CPT_NAME ) ) : ?>

      <h2>Nothing found</h2>
			<p>There are currently no project. Please check back later.</p>

		<?php elseif( is_post_type_archive( PL_PRODUCTS_CPT_NAME ) ) : ?>

      <h2>Nothing found</h2>
			<p>There are currently no stories to show. Please check back later.</p>

		<?php elseif ( is_search() ) : ?>

      <h2>Nothing found</h2>
			<p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>

		<?php elseif ( is_home() ) : ?>

			<p>There are currently no articles to show. Please check back later.</p>

			<?php if ( current_user_can( 'publish_posts' ) ) : ?>

				<p>Ready to publish your first post? <a href="<?php echo esc_url( admin_url( 'post-new.php' ) ); ?>">Get started here</a>.</p>

			<?php endif;
		
		// Fallback
		else : ?>

			<p>It seems we can't find what you're looking for. Perhaps searching can help.</p>

			<?php get_search_form();

    endif; ?>
    
	</div>
</div>
