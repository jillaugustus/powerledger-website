<?php 

//=============================================================================================================
// Formatted debug log
//=============================================================================================================

if( !function_exists( 'write_log' ) ) {
	function write_log ( $log ) {
		if ( true === WP_DEBUG ) {
			if ( is_array( $log ) || is_object( $log ) ) {
				error_log( PHP_EOL . '------- BEGIN DEBUGGING -------' . PHP_EOL . print_r( $log, true ) . PHP_EOL . '------- END DEBUGGING -------'  . PHP_EOL );
			} else {
				error_log( PHP_EOL . '------- BEGIN DEBUGGING -------' . PHP_EOL . $log . PHP_EOL . '------- END DEBUGGING -------'  . PHP_EOL );
			}
		}
	}
}




//=============================================================================================================
// Customise the login screen
//=============================================================================================================

function pl_custom_login_css() {
	echo '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/styles/login.css" />';
}
add_action('login_head', 'pl_custom_login_css');

function pl_login_logo_url() {
	return home_url();
}
add_filter( 'login_headerurl', 'pl_login_logo_url' );

function pl_login_logo_url_title() {
	return get_bloginfo( 'name' );
}
add_filter( 'login_headertext', 'pl_login_logo_url_title' );




//=============================================================================================================
// pl_which_template - displays which theme file is being used
//=============================================================================================================

// Displays which theme file is being used
function pl_which_template() {

	if ( is_admin() ) return;

	if ( !is_user_logged_in() ) return;

	if( stristr( $_SERVER['HTTP_HOST'], 'local.' ) || stristr( $_SERVER['HTTP_HOST'], 'staging.' ) ) {
		global $template;
		$template_name = substr( $template, ( strpos( $template, 'wp-content/') + 10 ) );
		echo '<div style="position: fixed; bottom: 0; background-color: #ffffff; border-radius: 0 5px 0 0; color: #000000; font-family: courier,serif; font-size: 11px; left: 0; padding: 8px 15px; z-index: 999;}">Template used on this page: ' . $template_name . '</div>';
	}
}




//=============================================================================================================
// pl_big_image_size_threshold - set a new size for the new WP 5.3 automatic image scaler
//=============================================================================================================

/**
 * pl_big_image_size_threshold
 * 
 * Set a new size for the new WP 5.3 automatic image scaler
 */
function pl_big_image_size_threshold( $imagesize, $file, $attachment_id ){
	return 4000; // Max image width
}
add_filter( 'big_image_size_threshold', 'pl_big_image_size_threshold', 10, 3 );




//=============================================================================================================
// pl_radio_button - creates a nicer looking radio button
//=============================================================================================================

function pl_radio_button($field_id = '', $option_id = '', $option_value = '', $label = '', $checked = false ) {

	$output = '';

	if(isset($field_id) && isset($option_id) && isset($option_value)) {

		// Open wrapper
		$output .= '<div class="radio-option">';

		// Open input
		$output .= '<input id="' . $option_id . '" name="' . $field_id . '" type="radio" value="' . $option_value . '"';
		
		if($checked) {
			$output .= ' checked';
		}

		// Close input
		$output .= '/>';

		// Create the imitation radio button
		$output .= '<div class="radio"><div class="checker"></div></div>';
		
		// Open label
		$output .= '<label for="' . $option_id . '">';

		if($label) {
			$output .= '<span>' . $label . '</span>';
		}

		// Close label
		$output .= '</label>';

		// Close wrapper
		$output .= '</div>';
	}

	echo $output; 
}




//=============================================================================================================
// pl_template_remove_content_editor - remove the content editor from pages/templates that don't need it
//=============================================================================================================

function pl_template_remove_content_editor() {

	if( isset($_GET['post']) ) {
		
		$id = $_GET['post'];
		$template = get_post_meta($id, '_wp_page_template', true);

		// Remove editor using certain page templates
		switch ($template) {

			case 'templates/media-centre.php':
			case 'templates/about.php':
			case 'templates/contact.php':
			case 'templates/products.php':
			case 'templates/current-projects.php':
			case 'templates/technology.php':
			case 'templates/careers.php':
			case 'templates/vpp-calculator.php':
			case 'templates/values.php':
			case 'templates/vpp-registration.php':
			case 'templates/vpp-registration-sa.php':
			case 'templates/age-coming-soon.php':
			case 'templates/press.php':
				remove_post_type_support('page', 'editor');
			break;

			default :
				// Don't remove on any other templates
			break;
		}

		// Remove editor from pages set as dummy archive for CPTs
		switch($id) {

			case get_option('page_for_' . PL_PROJECTS_CPT_NAME ):
			case get_option('page_for_' . PL_PRODUCTS_CPT_NAME ):
			case get_option('page_for_' . PL_JOBS_CPT_NAME ):
				remove_post_type_support('page', 'editor');
			break;

			default :
				// Don't remove on any other pages
			break;
		}
	}
}
add_action('init', 'pl_template_remove_content_editor');




//=============================================================================================================
// pl_admin_page_template_column - show each pages active template in the admin pages list view
//=============================================================================================================

function pl_admin_page_template_column( $defaults ) {
	$defaults['page-layout'] = __('Template');
	return $defaults;
}

function pl_admin_page_template_column_content( $column_name, $id ) {
	if ( $column_name === 'page-layout' ) {
		$set_template = get_post_meta( get_the_ID(), '_wp_page_template', true );

		// if ( $set_template == 'default' ) {
		// 	echo 'Default';
		// }

		$templates = get_page_templates();
		ksort( $templates );
		foreach ( array_keys( $templates ) as $template ) :
			if ( $set_template == $templates[$template] ) echo $template;
		endforeach;
	}
}

add_filter( 'manage_pages_columns', 'pl_admin_page_template_column' );
add_action( 'manage_pages_custom_column', 'pl_admin_page_template_column_content', 5, 2 );




//=============================================================================================================
// pl_remove_default_image_sizes - remove unwanted image sizes generated by WP
//=============================================================================================================

function pl_remove_default_image_sizes( $sizes ) {
	unset($sizes['1536x1536']);
	unset($sizes['2048x2048']);
	return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'pl_remove_default_image_sizes');
