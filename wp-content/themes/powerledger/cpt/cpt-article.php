<?php

//=============================================================================================================
// Define constant variables
//=============================================================================================================

// Define constants
define( 'PL_ARTICLES_CPT_NAME', 'article' );
define( 'PL_ARTICLES_SINGLE_NAME', 'Article' );
define( 'PL_ARTICLES_PLURAL_NAME', 'Articles' );

// Define permalinks
$archive_page_id = get_option( 'page_for_' . PL_ARTICLES_CPT_NAME );
$archive_page_slug = str_replace( home_url(), '', get_permalink( $archive_page_id ) );
$archive_permalink = ( $archive_page_id ? $archive_page_slug : 'article' );
$archive_permalink = ltrim( $archive_permalink, '/' );
$archive_permalink = rtrim( $archive_permalink, '/' );
define( 'PL_ARTICLES_REWRITE_SLUG', $archive_permalink );




//=============================================================================================================
// Register CPT
//=============================================================================================================

function pl_articles_register_cpt() {

	$labels = array(
		'name'                      => PL_ARTICLES_PLURAL_NAME,
		'singular_name'             => PL_ARTICLES_SINGLE_NAME,
		'menu_name'                 => PL_ARTICLES_PLURAL_NAME,
		'name_admin_bar'            => PL_ARTICLES_SINGLE_NAME,
		'archives'              		=> 'Article archives',
		'attributes'            		=> 'Article attributes',
		'parent_item_colon'     		=> 'Parent article:',
		'all_items'             		=> 'All articles',
		'add_new_item'          		=> 'Add new article',
		'add_new'               		=> 'Add new article',
		'new_item'              		=> 'New article',
		'edit_item'             		=> 'Edit article',
		'update_item'           		=> 'Update article',
		'view_item'             		=> 'View article',
		'view_items'            		=> 'View articles',
		'search_items'          		=> 'Search article',
		'not_found'             		=> 'Not found',
		'not_found_in_trash'    		=> 'Not found in trash',
		'featured_image'        		=> 'Featured Image',
		'set_featured_image'    		=> 'Set featured image',
		'remove_featured_image' 		=> 'Remove featured image',
		'use_featured_image'    		=> 'Use as featured image',
		'insert_into_item'      		=> 'Insert into article',
		'uploaded_to_this_item' 		=> 'Uploaded to this article',
		'items_list'            		=> 'Articles list',
		'items_list_navigation' 		=> 'Articles list navigation',
		'filter_items_list'     		=> 'Filter articles list'
	);

	$rewrite = array(
		'slug'                  		=> 'article', // The slug for single posts
		'with_front'            		=> false,
		'pages'                 		=> true,
		'feeds'                 		=> false
	);
	
	$args = array(
		'label'                 		=> PL_ARTICLES_CPT_NAME,
		'description'           		=> 'Content for a single Power Ledger article',
		'labels'                		=> $labels,
		'supports'              		=> array( 'title', 'editor', 'thumbnail', 'revisions', 'author' ),
		'taxonomies'            		=> array(),
		'hierarchical'          		=> false,
		'public'                		=> true,
		'show_ui'               		=> true,
		'show_in_menu'          		=> true,
		'menu_position'         		=> 20,
		'menu_icon'             		=> 'dashicons-text-page',
		'show_in_admin_bar'     		=> true,
		'show_in_nav_menus'     		=> true,
		'can_export'            		=> true,
		'has_archive'           		=> PL_ARTICLES_REWRITE_SLUG, // The slug for archive
		'exclude_from_search'   		=> false,
		'publicly_queryable'    		=> true,
		'capability_type'       		=> 'page',
		'rewrite'										=> $rewrite
	);
	register_post_type( PL_ARTICLES_CPT_NAME, $args );

}
add_action( 'init', 'pl_articles_register_cpt', 0 );




// Change CPT title placeholder on edit screen
function pl_articles_cpt_title_placeholder( $title, $post ) {

	if ( $post->post_type == PL_ARTICLES_CPT_NAME ) {
		return 'Add article title';
	}
	return $title;
}
add_filter( 'enter_title_here', 'pl_articles_cpt_title_placeholder', 10, 2 );
