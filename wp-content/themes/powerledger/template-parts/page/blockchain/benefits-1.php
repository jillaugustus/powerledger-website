<?php

// Variables
$title = get_field('blockchain_benefits_1_title');
$text = get_field('blockchain_benefits_1_text');

if( $text && $title ) : ?>

  <section class="section section-blockchain-benefits pb0">
    <div class="container">

    <div class="content-wrap">
        <div class="entry-content">
          <div class="entry-content-wrap">
            <h2><?php echo $title ?></h2>
            <?php echo $text ?>
          </div>
        </div>
      </div>
      
    </div>
  </section>

<?php endif;
