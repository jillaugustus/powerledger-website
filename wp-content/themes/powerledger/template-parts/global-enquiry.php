<?php

$modifier = '';

// Templates to use the bg image style
$graphic_left_style = array(
  // 'templates/products.php',
  // 'templates/media-centre.php',
  // 'page-projects.php',
  // 'page-technology.php',
  // 'page.php',
);

// Templates to use the right graphic style
$graphic_right_style = array(
  'templates/about.php',
);

// All others use left graphic style

if ( is_page_template( $graphic_left_style ) || is_front_page() ) {
  $modifier = ' bg-device-left';
} elseif ( is_page_template( $graphic_right_style ) ) {
  $modifier = ' bg-device-right';
} else {
  $modifier = ' bg-texture';
}

?>

<section class="section-enquiry<?php echo $modifier ?>">
  <div class="container">
    <h2>Make an enquiry</h2>
    <p>If you have questions or comments, feel free to get in touch with us using our enquiry form.</p>
    <a href="#enquiry-form-general" class="button modaal-trigger">Enquiry form</a>
  </div>
</section>
