<?php 

// Variables
$title = get_field('vpp_calculator_intro_title');
$text = get_field('vpp_calculator_intro_text');

?>

<section class="section section-full-width section-calculator-intro">
  <div class="container">
    <div class="intro-column">
      <h2><?php echo $title ?></h2>
      <?php if($text) :
        echo $text;
      endif; ?>
    </div>
  </div>
</section>