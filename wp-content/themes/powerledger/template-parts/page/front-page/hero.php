<?php 

// Variables
$title = get_field('hero_title');
$sub_title = get_field('hero_subtitle');
$video_mp4 = get_field('hero_video_mp4');
$video_webm = get_field('hero_video_webm');
$cta_bar_text = get_field('cta_bar_text');
$cta_bar_link = get_field('cta_bar_link');
$products_archive_link = get_permalink(get_option('page_for_product'));

?>

<section class="section-generic-hero section-home-hero">
  <div class="hero-content">
    <h1 class="entry-title big"><?php echo $title ?></h1>
    <span class="entry-sub-title h3"><?php echo $sub_title ?></span>

    <div class="actions">
      <div class="button-group">
        <a href="#enquiry-form-general" class="button modaal-trigger">Make an enquiry</a>
        <?php if( $products_archive_link ) : ?>
          <a href="<?php echo $products_archive_link ?>" class="button">Our technology</a>
        <?php endif; ?>
      </div>
    </div>

    <?php
    $args = array(
      'post_type'         => PL_ANNOUNCEMENTS_CPT_NAME,
      'posts_per_page'    => '1',
    );
    $announcements_loop = new WP_Query($args);
    
    if( $announcements_loop->have_posts() ):
      while( $announcements_loop->have_posts() ) : $announcements_loop->the_post(); 
      
        $custom_title = get_field('page_custom_title');
        $title = ($custom_title) ? $custom_title : get_the_title();
      
        ?>

        <div class="hero-news-teaser">
          <a href="<?php the_permalink(); ?>"><span>Latest: </span><?php echo $title ?></a>
        </div>

      <?php endwhile; wp_reset_postdata(); ?>
    <?php endif; ?>
  </div>

  <video playsinline autoplay muted loop>
    <source src="<?php echo $video_mp4 ?>" type="video/mp4">
    <source src="<?php echo $video_webm ?>" type="video/webm">
    <track src="<?php echo get_template_directory_uri() . '/files/captions_en.vtt'; ?>" kind="captions" srclang="en" label="english_captions">
  </video>

  <?php if(!empty($cta_bar_text)) : ?>
    <div class="cta-bar">
      <p>
        <?php echo $cta_bar_text ?>
        <?php if(!empty($cta_bar_link)): ?>
        &emsp;
        <a href="<?php echo $cta_bar_link ?>" class="button" target="_blank">Register here</a>
        <?php endif; ?>
      </p>
    </div>
  <?php endif; ?>
</section>