<div id="enquiry-form-general" class="modaal-content-source">
  <h2 class="h3">Enquiry form</h2>
  <!-- Embedding hubspot contact form -->
  <!--[if lte IE 8]>
  <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
  <![endif]-->
  <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
  <script>
    hbspt.forms.create({
      portalId: "4519667",
      formId: "a24b7ca2-a968-4faf-9ecc-b89b8ab3c54e"
    });
  </script>
</div>
