<?php 

// Variables
$image = wp_get_attachment_image_src( get_field('our_why_image'), 'large' )[0];
$title = get_field('our_why_title');
$text = get_field('our_why_text');
$link = get_field('values_link');

?>

<section class="section section-about-why">
  <div class="container">
    <div class="section-column section-image-cover" style="background-image: url('<?php echo $image ?>');"></div>

    <div class="section-column section-content">
      <div class="content-wrap">
        
        <h2><?php echo $title ?></h2>

        <?php if($text) : echo $text; endif; ?>
        
        <?php if($link) : ?>
          <a href="<?php echo $link ?>" class="button">Read our values</a>
        <?php endif; ?>

      </div>
    </div>
  </div>
</section>