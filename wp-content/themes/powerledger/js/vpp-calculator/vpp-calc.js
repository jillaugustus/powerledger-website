jQuery(function ($) {
  // regions (city)
  const regionData = [
    { value: 'sa', data: 4.75, lookUp: saLookUpData },
    { value: 'nsw', data: 4.6, lookUp: nswLookUpData },
    { value: 'qld', data: 5.1, lookUp: qldLookUpData },
    { value: 'vic', data: 4.2, lookUp: vicLookUpData }
  ];

  // battery providers
  const batteryData = [
    {
      value: 'battery-1',
      batteryCapacity: 4.5,
      minBattery: 0,
      batterySoc: 2.3,
      batteryPower: 2.5
    },
    {
      value: 'battery-2',
      batteryCapacity: 6.75,
      minBattery: 0,
      batterySoc: 3.4,
      batteryPower: 3.3
    },
    {
      value: 'battery-3',
      batteryCapacity: 9,
      minBattery: 0,
      batterySoc: 4.5,
      batteryPower: 3.3
    },
    {
      value: 'battery-4',
      batteryCapacity: 11.25,
      minBattery: 0,
      batterySoc: 5.6,
      batteryPower: 3.3
    },
    {
      value: 'battery-5',
      batteryCapacity: 13.5,
      minBattery: 0,
      batterySoc: 6.8,
      batteryPower: 3.3
    },
    {
      value: 'battery-6',
      batteryCapacity: 5.76,
      minBattery: 0,
      batterySoc: 2.9,
      batteryPower: 3
    },
    {
      value: 'battery-7',
      batteryCapacity: 7.68,
      minBattery: 0,
      batterySoc: 3.8,
      batteryPower: 3.3
    },
    {
      value: 'battery-8',
      batteryCapacity: 9.6,
      minBattery: 0,
      batterySoc: 4.8,
      batteryPower: 3.3
    },
    {
      value: 'battery-9',
      batteryCapacity: 11.52,
      minBattery: 0,
      batterySoc: 5.8,
      batteryPower: 3.3
    },
    {
      value: 'battery-10',
      batteryCapacity: 13.44,
      minBattery: 0,
      batterySoc: 6.7,
      batteryPower: 3.3
    },
    {
      value: 'battery-11',
      batteryCapacity: 15.36,
      minBattery: 0,
      batterySoc: 7.7,
      batteryPower: 3.3
    },
    {
      value: 'battery-12',
      batteryCapacity: 13.5,
      minBattery: 0,
      batterySoc: 6.8,
      batteryPower: 5
    },
    {
      value: 'battery-13',
      batteryCapacity: 9,
      minBattery: 0,
      batterySoc: 4.5,
      batteryPower: 3.5
    }
  ];

  // formats and rounds the given amount to a specific number of decimals
  function formatAmount(amount, dec) {
    var dec = 2;
    var power = Math.pow(10, dec);
    return parseFloat((Math.round(parseFloat(amount) * power) / power).toFixed(dec));
  }

  // calcualte the battery's new soc given the charge delta based on the battery's current state
  function calcBatterySoc(batteryStats, chargeDelta, soc) {
    const { batteryCapacity, minBattery, batteryPower } = batteryStats;
    const halfBatteryPower = batteryPower / 2;
    var newChargeDelta = chargeDelta;
    var newSoc = soc;

    // ensures rate of charge and discharge is below battery power
    if (Math.abs(chargeDelta) > halfBatteryPower) {
      newChargeDelta = (chargeDelta / Math.abs(chargeDelta)) * halfBatteryPower;
    }

    // charge the battery given current battery's soc and charge delta
    newSoc = soc + newChargeDelta;

    // new soc cannot exceed battery capacity, use maximum
    if (newSoc > batteryCapacity) {
      newSoc = batteryCapacity;
    }

    // new soc cannot dip below minimum battery capacity, use minimum
    if (newSoc < minBattery) {
      newSoc = minBattery;
    }

    return newSoc;
  }

  // main VPP calculation logic
  function vppCalc() {
    // input variables
    var dailyUsage = parseFloat($('#daily-usage').val());
    var solarPower = parseFloat($('#solar-power').val());
    var region = $('input[name=region]:checked').val();
    var battery = $('option[name=battery]:selected').val();


    // gets the region average daily solar exposure based on selected option
    const selectedRegion = regionData.filter(v => v.value == region);
    const regionSolar = (selectedRegion.length) ? selectedRegion[0].data : 0;
    const regionLookup = (selectedRegion.length) ? selectedRegion[0].lookUp : [];

    // gets the battery stats based on selected option
    var batteryStats = batteryData.filter(v => v.value == battery);
    batteryStats = (batteryStats.length) ? batteryStats[0] : {
      batteryCapacity: 0, minBattery: 0, batterySoc: 0, batteryPower: 0
    }
    const { batterySoc, batteryPower } = batteryStats;

    // Temporary constant variables (from sonnen)
    const feedInTariff = 0.115; // rate sold to grid
    const gridBuyRate = 0.3;

    // imports data from the daily average solar demand
    // contains 48 data points, 24 hours, 30 minutes settlement interval
      var solarDemand = [];

      // calculates the generation and consumption for each interval based on user inputs
      $.each(solarData, function (key, val) {
        var solar = val.solar * regionSolar; // region average solar
        var load = val.load * dailyUsage; // user daily household consumption
        var generation = solar * solarPower; // user solar factor
        var solarConsumption = Math.min(load, generation); // minimum solar consumption
        var excessGeneration = generation - solarConsumption; // remaining generated solar

        solarDemand[key] = {
          solar: formatAmount(solar, 5),
          load: formatAmount(load, 5),
          generation: formatAmount(generation, 5),
          solarConsumption: formatAmount(solarConsumption, 5),
          excessGeneration: formatAmount(excessGeneration, 5)
        };
      });

      // after calculating the daily solar demand profile, get the region's pricing information
        var workingTable = [];

        var battSoc = batterySoc; // starting battery soc
        var battDischarge = 0;
        var vppExport = 0;
        var selfConsumption = 0;

        $.each(regionLookup, function (k, v) {
          const previousEntry = (k > 0) ? workingTable[k - 1] : null; // previous interval for comparison
          const solarIndex = k % 48; // if k is 48, reset to 0 as the first interval in the solar demand
          const currentSolar = solarDemand[solarIndex];

          const gridRate = formatAmount(gridBuyRate * 1000, 3); // trigger rate for VPP events
          // calculates the highest price based on the 7 pricing data and the grid rate
          const highestPrice = Math.max(...Object.keys(v).map(x => v[x]), gridRate);

          // if grid rate is higher than the region pricing data, then it is self-consumption
          const bestAction = (gridRate >= highestPrice) ? 'self_consumption' : 'RRP';
          const bestPrice = Math.max(highestPrice, 1);
          // is VPP decision if best action is from the one of the region's pricing data
          const vppDecision = (bestAction === 'RRP') ? 1 : 0;

          // calculates the current interval's battery charge after each solar consumption
          const battCharge = currentSolar.generation - currentSolar.solarConsumption;

          const previousBattSoc = (previousEntry) ? parseFloat(previousEntry.battSoc) : 0;

          // if VPP decision, calculate the VPP export based on the amount discharged
          // battery discharged is based on the previous battery's soc or half the battery power
          if (vppDecision) {
            selfConsumption = 0;
            battDischarge = Math.min(previousBattSoc, batteryPower / 2);
            vppExport = formatAmount(battDischarge, 5);
          } else {
            // action is self-consumption, battery discharge is based on the previous battery's soc or
            // the remaining solar after consumption
            selfConsumption = Math.min(
              previousBattSoc,
              (currentSolar.load - currentSolar.solarConsumption)
            );

            battDischarge = selfConsumption;
            vppExport = 0;
          }

          // potential battery charge (kW) after discharge
          const chargeDelta = battCharge - battDischarge;

          // calculates the battery's new soc if there is a previous entry to be based on
          if (previousEntry) {
            battSoc = calcBatterySoc(batteryStats, parseFloat(chargeDelta), previousBattSoc);
          }

          // calculates the Feed-in Tariff (kW) given the remaining solar generation and battery's soc
          const fit = Math.max(
            currentSolar.excessGeneration - Math.abs(previousBattSoc - battSoc),
            0
          );

          // calculates the self-consumption ($) based on the grid buy-rate
          // calculates the Feed-in Tariff earnings ($) based on the feed-in-tariff rate
          // calculates the VPP earnings based on the highest price from the region's pricing data
          const bSelfConsumption = formatAmount(selfConsumption * gridBuyRate, 10);
          const bFitEarnings = formatAmount(fit * feedInTariff, 10);
          const bVppEarnings = formatAmount(vppExport * (bestPrice / 1000), 10);

          // summaries the amounts and tracks the battery's stats for each interval
          workingTable[k] = {
            ...v,
            ...currentSolar,
            battSoc: formatAmount(battSoc, 5),
            battCharge: formatAmount(battCharge, 5),
            battDischarge: formatAmount(battDischarge, 5),
            chargeDelta: formatAmount(chargeDelta, 5),
            selfConsumption: formatAmount(selfConsumption, 5),
            fit: formatAmount(fit, 4),
            vppExport, bestAction, bestPrice, vppDecision,
            bSelfConsumption, bFitEarnings, bVppEarnings
          };
        });

        // after calculating the entire year, calculate sum of total VPP decisions
        var numVppDecisions = workingTable.filter(v => v.vppDecision).length;

        // based on the entire year's interval,
        // calculate the totals ($) for self-consumption, Feed-in-Tariff earnings and VPP earnings
        var benefits = {
          ...workingTable.reduce((acc, v) => ({
            selfConsumption: acc.selfConsumption + parseFloat(v.bSelfConsumption),
            fitEarnings: acc.fitEarnings + parseFloat(v.bFitEarnings),
            vppEarnings: acc.vppEarnings + parseFloat(v.bVppEarnings),
            load: acc.load + parseFloat(v.load)
          }), { selfConsumption: 0, fitEarnings: 0, vppEarnings: 0, load: 0 }),
          bestAction: (numVppDecisions > (workingTable.length / 2)) ? 'RRP' : 'Self-consumption'
        };


        // display outcomes based on the benefits summary in AUD currency format
        $('#result-self-consumption').text(formatAmount(benefits.selfConsumption, 2).toLocaleString('en-AU', {style:'currency', currency:'AUD'}));
        $('#result-fit').text(formatAmount(benefits.fitEarnings, 2).toLocaleString('en-AU', {style:'currency', currency:'AUD'}));
        $('#result-vpp').text(formatAmount(benefits.vppEarnings, 2).toLocaleString('en-AU', {style:'currency', currency:'AUD'}));
        $('#result-total').text(formatAmount(
          benefits.selfConsumption + benefits.fitEarnings + benefits.vppEarnings
          , 2).toLocaleString('en-AU', {style:'currency', currency:'AUD'}));

        // Store selected battery values
        var selectedBattery = $('#batteries option:selected').val(),
            selectedBatteryName = $('#batteries option:selected').text();

        // Display battery message if certain battery is selected
        if((selectedBattery === 'battery-12') || (selectedBattery === 'battery-13')) {
          $('#selected-battery').text(selectedBatteryName);
          $('#battery-option-alert').removeClass('hide');
        } else {
          $('#battery-option-alert').addClass('hide');
        }
  }


  // Click function
  $('#calc-submit').on('click', function () {
    var results_section = '.section-vpp-calculator-results';

    // Run the calculation
    vppCalc();


    // Expand the results are if isn't already
    if ($(results_section).is(':hidden')) {
      $(results_section).slideDown('500');
    }

    // Scroll to the results area
    $('html, body').animate({ scrollTop: ($(results_section).offset().top) - 200 }, 500);
  });

});
