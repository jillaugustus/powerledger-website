<?php

//=============================================================================================================
// Define constant variables
//=============================================================================================================

// Define constants
define( 'PL_JOBS_CPT_NAME', 'job' );
define( 'PL_JOBS_SINGLE_NAME', 'Job' );
define( 'PL_JOBS_PLURAL_NAME', 'Jobs' );

// Define permalinks
$archive_page_id = get_option( 'page_for_' . PL_JOBS_CPT_NAME );
$archive_page_slug = str_replace( home_url(), '', get_permalink( $archive_page_id ) );
$archive_permalink = ( $archive_page_id ? $archive_page_slug : 'jobs' );
$archive_permalink = ltrim( $archive_permalink, '/' );
$archive_permalink = rtrim( $archive_permalink, '/' );
define( 'PL_JOBS_REWRITE_SLUG', $archive_permalink );




//=============================================================================================================
// Register CPT
//=============================================================================================================

function pl_jobs_register_cpt() {

	$labels = array(
		'name'                      => PL_JOBS_PLURAL_NAME,
		'singular_name'             => PL_JOBS_SINGLE_NAME,
		'menu_name'                 => PL_JOBS_PLURAL_NAME,
		'name_admin_bar'            => PL_JOBS_SINGLE_NAME,
		'archives'              		=> 'Job archives',
		'attributes'            		=> 'Job attributes',
		'parent_item_colon'     		=> 'Parent job:',
		'all_items'             		=> 'All jobs',
		'add_new_item'          		=> 'Add new job',
		'add_new'               		=> 'Add new job',
		'new_item'              		=> 'New job',
		'edit_item'             		=> 'Edit job',
		'update_item'           		=> 'Update job',
		'view_item'             		=> 'View job',
		'view_items'            		=> 'View jobs',
		'search_items'          		=> 'Search job',
		'not_found'             		=> 'Not found',
		'not_found_in_trash'    		=> 'Not found in Trash',
		'featured_image'        		=> 'Featured Image',
		'set_featured_image'    		=> 'Set featured image',
		'remove_featured_image' 		=> 'Remove featured image',
		'use_featured_image'    		=> 'Use as featured image',
		'insert_into_item'      		=> 'Insert into job',
		'uploaded_to_this_item' 		=> 'Uploaded to this job',
		'items_list'            		=> 'Jobs list',
		'items_list_navigation' 		=> 'Jobs list navigation',
		'filter_items_list'     		=> 'Filter jobs list'
	);

	$rewrite = array(
		'slug'                  		=> 'job', // The slug for single posts
		'with_front'            		=> false,
		'pages'                 		=> true,
		'feeds'                 		=> false
	);

	$args = array(
		'label'                 		=> PL_JOBS_SINGLE_NAME,
		'description'           		=> 'Content for a single Power Ledger job',
		'labels'                		=> $labels,
		'supports'              		=> array( 'title', 'editor', 'thumbnail', 'revisions' ),
		'taxonomies'            		=> array(),
		'hierarchical'          		=> false,
		'public'                		=> true,
		'show_ui'               		=> true,
		'show_in_menu'          		=> true,
		'menu_position'         		=> 20,
		'menu_icon'             		=> 'dashicons-businessman',
		'show_in_admin_bar'     		=> true,
		'show_in_nav_menus'     		=> true,
		'can_export'            		=> true,
		'has_archive'           		=> PL_JOBS_REWRITE_SLUG, // The slug for archive
		'exclude_from_search'   		=> false,
		'publicly_queryable'    		=> true,
		'capability_type'       		=> 'page',
		'rewrite'										=> $rewrite
	);
	register_post_type( PL_JOBS_CPT_NAME, $args );

}
add_action( 'init', 'pl_jobs_register_cpt', 0 );




// Change CPT title placeholder on edit screen
function pl_jobs_cpt_title_placeholder( $title, $post ) {

	if ( $post->post_type == PL_JOBS_CPT_NAME ) {
		return 'Add job title';
	}
	return $title;
}
add_filter( 'enter_title_here', 'pl_jobs_cpt_title_placeholder', 10, 2 );




// Change the number of 'posts per page' for the CPT archive
function pl_jobs_cpt_modify_archive_query( $query ) {

	if ( $query->is_post_type_archive( PL_JOBS_CPT_NAME ) && !is_admin() && $query->is_main_query() ) {
		$query->set( 'posts_per_page', '-1' );
	}

	return $query;
}
add_filter( 'pre_get_posts', 'pl_jobs_cpt_modify_archive_query' );
