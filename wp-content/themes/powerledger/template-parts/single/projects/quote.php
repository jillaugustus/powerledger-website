<?php 

// Variables
$text = get_field('project_quote');
$author = get_field('project_quote_author');

if( $text && $author ) : ?>

  <section class="section section-testimonial section-project-quote">
    <div class="container">
      <h3 class="quote big"><?php echo $text ?></h3>
      <strong class="attribution"><?php echo $author ?></strong>
    </div>
  </section>

<?php endif;
