<?php
// Register Custom Taxonomy
function continent() {

	$labels = array(
		'name'                       => _x( 'Continents', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Continent', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Continent', 'text_domain' ),
		'all_items'                  => __( 'All Continents', 'text_domain' ),
		'parent_item'                => __( 'Parent Continent', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Continent:', 'text_domain' ),
		'new_item_name'              => __( 'New Continent Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Continent', 'text_domain' ),
		'edit_item'                  => __( 'Edit Continent', 'text_domain' ),
		'update_item'                => __( 'Update Continent', 'text_domain' ),
		'view_item'                  => __( 'View Continent', 'text_domain' ),
		'separate_items_with_commas' => __( 'Seperate Continents with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or Remove Continents', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from Most Used', 'text_domain' ),
		'popular_items'              => __( 'Popular Continents', 'text_domain' ),
		'search_items'               => __( 'Search Continents', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No Continents', 'text_domain' ),
		'items_list'                 => __( 'Continents List', 'text_domain' ),
		'items_list_navigation'      => __( 'Continents list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'continent', array( 'project' ), $args );

}
add_action( 'init', 'continent', 0 );
?>
