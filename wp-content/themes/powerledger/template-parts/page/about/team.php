<?php 

// Variables
$image = wp_get_attachment_image_src( get_field('team_image'), 'large' )[0];
$text = get_field('team_text');
$link = get_field('team_link');

?>

<section class="section section-about-team bg-texture">
  <div class="container">
    <div class="section-column section-image-cover" style="background-image: url('<?php echo $image ?>');"></div>

    <div class="section-column section-content">
      <div class="content-wrap">
      
        <h2>Our team</h2>

        <?php if($text) : echo $text; endif; ?>

        <?php if($link): ?>
          <a href="<?php echo $link ?>" class="button">Team profiles</a>
        <?php endif; ?>

      </div>
    </div>
  </div>
</section>