<?php

//=============================================================================================================
// Define constant variables
//=============================================================================================================

define( 'PL_PRODUCT_CATEGORIES_TAX_NAME', 'product-categories' );




//=============================================================================================================
// Register taxonomy
//=============================================================================================================

function pl_product_categories_register_taxonomy() {

	$labels = array(
		'name'                       => 'Product Categories',
		'singular_name'              => 'Product Categories',
		'menu_name'                  => 'Product Categories',
		'all_items'                  => 'All product categories',
		'parent_item'                => 'Parent product category',
		'parent_item_colon'          => 'Parent product category:',
		'new_item_name'              => 'New Product Category Name',
		'add_new_item'               => 'Add New Product Category',
		'edit_item'                  => 'Edit Product Category',
		'update_item'                => 'Update Product Category',
		'view_item'                  => 'View Product Category',
		'separate_items_with_commas' => 'Separate product categories with commas',
		'add_or_remove_items'        => 'Add or remove product categories',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular Product Categories',
		'search_items'               => 'Search Product Categories',
		'not_found'                  => 'Not Found',
		'no_terms'                   => 'No product categories',
		'items_list'                 => 'Product Categories list',
		'items_list_navigation'      => 'Product Categories list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( PL_PRODUCT_CATEGORIES_TAX_NAME, PL_PRODUCTS_CPT_NAME, $args );

}
add_action( 'init', 'pl_product_categories_register_taxonomy', 0 );
