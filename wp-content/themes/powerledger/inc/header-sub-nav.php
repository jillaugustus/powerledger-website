<?php

//=============================================================================================================
// pl_header_sub_nav - lists parent page and all descendant pages as a menu
//=============================================================================================================

/**
* Sidebar submenu: lists parent page and all descendant pages as a menu
*
* Use wp_list_pages to display parent and all child pages all generations (a tree with parent)
*
* http://codex.wordpress.org/Template_Tags/wp_list_pages#List_Sub-Pages*
*/

function pl_header_sub_nav() {
    
  global $post;

  if( !$post ) {
    return;
  }

  $current_page_id = $post->ID;
  $child_posts = null;
  $cpt_page_id = null;

  $parent_id = pl_get_top_parent_page_id( $current_page_id );

  if( !$parent_id ) return;

  // Get the current parent post
  $parent_post = get_post( $parent_id );

  // Setup args to get child posts of the current parent that ARE set as 'action' items
  $action_child_args = array( 
    'post_parent'         => $parent_id, 
    'post_type'           => array('page', 'post', PL_PRODUCTS_CPT_NAME), 
    'post_status'         => 'publish',
    'order'               => 'DESC',
    'orderby'             => 'menu_order, post_title',
    'meta_query' => array(
      array(
        'key'               => 'page_menu_action',
        'value'             => TRUE,
        'compare'           => '=',
      ),
    ),
  );

  // Get child pages using the above arguments
  $action_child_posts = get_children( $action_child_args );

  // Setup args to get child posts of the current parent that ARE NOT set as 'action' items
  $basic_child_args = array( 
    'post_parent'         => $parent_id, 
    'post_type'           => array('page', 'post', PL_PRODUCTS_CPT_NAME), 
    'post_status'         => 'publish',
    'order'               => 'DESC',
    'orderby'             => 'menu_order, post_title',
    'meta_query' => array(
      array(
        'key'               => 'page_menu_action',
        'value'             => TRUE,
        'compare'           => '!=',
      ),
    ),
  );

  // Get child pages of the current parent
  $basic_child_posts = get_children( $basic_child_args );

  // Merge the parent and all child pages into a single array, with the parent at the front of the array and any pages set as an 'action' item at the end
  $all_posts = array_merge( array($parent_id), array_keys( $basic_child_posts ), array_keys( $action_child_posts ) );

  // Check if there any child posts before proceeding
  if( $basic_child_posts || $action_child_pages ) {

    $html = '<div class="header-bottom">';
    $html .= '<div class="container">';
    $html .= '<div class="parent-section-title"><span class="title h3">' . $parent_post->post_title . '</span></div>';
    $html .= '<ul class="sub-menu">';

    foreach ( $all_posts as $post_object ) {

      $post_object = get_post($post_object);

      // Setup unique classes for parent item and current item
      $current_page_class = '';
      $parent_class = '';
      $link_class = '';

      if( $post_object->ID === $current_page_id ) {
        $current_page_class = ' current-item';
      }

      if( $post_object->ID === $parent_id ) {
        $parent_class = ' parent-item';
      }

      // Get unique navigation labels and custom page titles
      $custom_title = get_field('page_custom_title', $post_object->ID);
      $menu_label = get_field('page_menu_label', $post_object->ID);
      $menu_action = get_field('page_menu_action', $post_object->ID);

      if($menu_action) {
        $link_class = ' button';
      }

      // Create menu items
      $html .= '<li class="sub-menu-item sub-menu-item-' . $post_object->ID . $parent_class . $current_page_class . '">';
      $html .= '<a class="' . $link_class . '" href="' . get_permalink( $post_object->ID ) . '">';
      
      // Set the menu label and fallbacks
      if( $menu_label ) {
        $html .= $menu_label;
      } elseif( $custom_title ) {
        $html .= $custom_title;
      } else {
        $html .= $post_object->post_title;
      }
      
      // Close menu item
      $html .= '<i class="current-marker"></i></a>';
      $html .= '</li>';

    }

    $html .= '</ul>';
    $html .= '</div>';
    $html .= '</div>';

    return $html;

  } 
  
  // If there are no child pages then return false
  else {
    return false;
  }
}




// Find top level page ID
function pl_get_top_parent_page_id( $id ) {

  global $post;
  $ancestors = $post->ancestors;

  // Check if page is a child page (any level)
  if ( $ancestors ) {
    return end( $ancestors ); // Grab the ID of top-level page from the tree
  } else {
    return $id; // Page is the top level, so use it's own id
  }
}
