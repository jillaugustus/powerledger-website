<?php 

// Variables
$image = wp_get_attachment_image_src( get_field('intro_image'), 'large' )[0];
$title = get_field('intro_title');
$text = get_field('intro_text');
$link = get_field('intro_link');

if( $title ) : ?>

  <section class="section-home-first bg-device-left">
    <div class="container">
      <div class="section-column">
        <h2><?php echo $title ?></h2>
        <?php if($text) : 
          echo $text; 
        endif; ?>
        <?php if($link) : ?>
          <a class="button" href="<?php echo $link ?>">More about us</a>
        <?php endif; ?>
      </div>

      <div class="section-column section-image-cover" style="background-image: url('<?php echo $image ?>');"></div>
    </div>
  </section>
  
<?php endif; 