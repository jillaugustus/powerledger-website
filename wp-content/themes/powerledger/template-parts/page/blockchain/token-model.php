<?php

// Variables 
$title = get_field('blockchain_token_model_title');
$text = get_field('blockchain_token_model_text');
$button = get_field('blockchain_token_model_button');

if( $title && $text ) : ?>

  <section class="section section-full-width bg-device-right">
    <div class="container">
      <div class="content-wrap">

        <h2><?php echo $title ?> </h2>
        <?php echo $text ?>

        <?php if( $button ) : ?>
          <a href="<?php echo $button['url'] ?>" class="button"<?php if( $button['target'] ) : ?> target="blank" rel="noopener"<?php endif; ?>><?php echo $button['title'] ?></a>
        <?php endif; ?>

      </div>
    </div>
  </section>

<?php endif;
