<?php 

// Variables
$image = wp_get_attachment_image_src( get_field('technology_image'), "article" )[0];
$title = get_field('technology_title');
$text = get_field('technology_text');
$link = get_field('technology_link');

?>

<section class="section-home-technology">
  <div class="container">

    <div class="section-column section-image-cover" style="background-image: url('<?php echo $image ?>');"></div>

    <div class="section-column">
      <h2><?php echo $title ?></h2>
      <?php if($text) : 
        echo $text;
      endif; ?>
      <?php if($link) : ?>
        <a class="button" href="<?php echo $link ?>">More about our technology</a>
      <?php endif; ?>
    </div>
    
  </div>
</section>