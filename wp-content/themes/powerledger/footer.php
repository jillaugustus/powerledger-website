<?php
/**
 * The template for displaying the footer
 *
 * @package powerledger
 */

?>

    </div>

    <footer class="footer">
      <div class="footer-subscribe">
        <div class="container">
          <div class="subscribe-wrap">
            <span>Sign up for the Power Ledger newsletter</span>
            <a href="#subscription-form" class="button modaal-trigger" aria-label="Open newsletter subscription form">Subscribe</a>
          </div>
        </div>
      </div>

      <div class="footer-top">
        <div class="container">
          <div class="footer-details">
            <div class="footer-branding">
              <a href="<?php echo get_home_url(); ?>" class="footer-logo" aria-hidden="true">
                <img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logo-powerledger.svg" alt="<?php bloginfo( 'name' ); ?>" />
              </a>
            </div>

            <div class="footer-contact">

              <address>
                <strong>Headquarters</strong><br />
                Level 2, The Palace<br />
                108 St Georges Terrace<br />
                Perth, Western Australia, 6000<br />
                +61 8 9322 6659
              </address>

              <?php get_template_part( 'template-parts/social-links' ); ?>

            </div>

            <div class="footer-sitemap">
              <nav class="footer-nav">
                <?php
                  wp_nav_menu( array(
                    'container'           => false,
                    'theme_location'      => 'main-menu',
                    'menu_id'             => 'footer-menu-1',
                    'fallback_cb'         => false,
                    'depth'               => 1
                  ) );
                  wp_nav_menu( array(
                    'container'           => false,
                    'theme_location'      => 'footer-menu-1',
                    'menu_id'             => 'footer-menu-2',
                    'fallback_cb'         => false,
                    'depth'               => 1
                  ) );
                  wp_nav_menu( array(
                    'container'           => false,
                    'theme_location'      => 'footer-menu-2',
                    'menu_id'             => 'footer-menu-3',
                    'fallback_cb'         => false,
                    'depth'               => 1
                  ) );
                ?>
              </nav>

            </div>
          </div>
        </div>
      </div>

      <div class="footer-bottom">
        <div class="container">
          <div class="footer-bottom-wrap">

            <nav class="footer-bottom-nav">
              <?php
              $footer_nav_args = array(
                'container'           => false,
                'theme_location'      => 'footer-bottom',
                'menu_class'          => 'footer-bottom-menu',
                'fallback_cb'         => false,
                'depth'               => 0
              );
              echo wp_nav_menu( $footer_nav_args ); 
              ?>
            </nav>

            <div class="copyright">
              <p>&copy; Power Ledger Pty Ltd <?php echo date( 'Y' ) ?></p>
            </div>

          </div>
        </div>
      </div>
    </footer>

    <?php 

    // Subscription modal content
    get_template_part('template-parts/modals/modal-subscription-form');

    // General enquiry modal content 
    get_template_part('template-parts/modals/modal-enquiry-form-general');

    // Sales enquiry modal content 
    get_template_part('template-parts/modals/modal-enquiry-form-sales');

    // Media enquiry modal content
    get_template_part('template-parts/modals/modal-enquiry-form-media');

    // VPP registration form modal content
    get_template_part('template-parts/modals/modal-vpp-registration-form');

    // Battery interest form
    get_template_part('template-parts/modals/modal-battery-interest-form');

    wp_footer();
    
    // Display the 'Which Template' helper for all environments except production
    // This function checks if the a user is logged in first too
    if( APPLICATION_ENV === 'development' || APPLICATION_ENV === 'staging' ) :
      pl_which_template();
    endif;
    
    ?>

  </body>
</html>
