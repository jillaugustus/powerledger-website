<?php
/**
 * Template name: VPP Registration
 * Template Post Type: page
 * 
 * @package powerledger
 */
if ( is_page('vpp-registration') ) {
  get_header( 'light' );
} else {
  get_header();
}
?>

  <?php if(get_field('hero_image')) { ?>
  <section class="section-generic-hero section-landing-hero" style="background-image: url('<?php echo wp_get_attachment_image_src( the_field('hero_image'), "full" )[0]; ?>');">
  <?php } else { ?>
  <section class="section-generic-hero">
  <?php } ?>
    <div class="inner-wrap">
      <div class="landing-content">
        <h1><?php the_field('page_custom_title'); ?></h1>
        <p><?php the_field('page_subtitle'); ?></p>
      </div>

      <div id="registration-form" class="form">
        <h2>Register your interest</h2>

        <div class="form-inner">
        <!-- Embedding hubspot contact form -->
        <!--[if lte IE 8]>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
        <![endif]-->
          <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
          <script>
            hbspt.forms.create({
            portalId: "4519667",
            formId: "c9133c9a-83f8-4e5e-b389-f0a08d519bb1"
          });
          </script>

          <p>By submitting this form, you agree to be contacted by Power Ledger representatives and partners for further information.</p>
        </div>
      </div>
    </div>
  </section>

  <section class="section-landing-video bg-texture">
    <div class="inner-wrap">
      <div class="content">
        <?php the_field('video'); ?>
      </div>
    </div>
  </section>

  <section class="section section-landing-features bg-device-left">
    <div class="inner-wrap">
      <div class="content">
        <h2><?php the_field('content_title'); ?></h2>
        <?php the_field('content'); ?>

        <div class="content-features">
          <div class="feature-item" style="background-image: url('<?php echo wp_get_attachment_image_src( the_field('feature_1_icon'), "full" )[0]; ?>');">
            <h3><?php the_field('feature_1_title'); ?></h3>
            <p><?php the_field('feature_1'); ?></p>
          </div>

          <div class="feature-item" style="background-image: url('<?php echo wp_get_attachment_image_src( the_field('feature_2_icon'), "full" )[0]; ?>');">
            <h3><?php the_field('feature_2_title'); ?></h3>
            <p><?php the_field('feature_2'); ?></p>
          </div>

          <div class="feature-item" style="background-image: url('<?php echo wp_get_attachment_image_src( the_field('feature_3_icon'), "full" )[0]; ?>');">
            <h3><?php the_field('feature_3_title'); ?></h3>
            <p><?php the_field('feature_3'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php

if ( is_page('vpp-registration') ) {
  get_footer( 'light' );
} else {
  get_footer();
}