<?php

// Variables
$archive_page_id = get_post_type_archive_page_id();

// Get taxonomy
$taxonomy = get_terms( PL_PRODUCT_CATEGORIES_TAX_NAME );
$count = count( $taxonomy );

// Setup counter
$i = 1;

foreach( $taxonomy as $term ) : 
  if( have_posts() ) : ?>

    <section id="<?php echo $term->slug ?>" class="section-products<?php echo ($i == 1) ? ' bg-device-left' : ''; echo ($i != $count) ? ' pb0' : ''; ?>">
      <div class="container">

        <div class="product-group-overview">
          <h2><?php echo $term->name; ?></h2>
          <?php the_field('product_category_' . $i . '_text', $archive_page_id); ?>
        </div>

        <div class="products-list">
          <?php while( have_posts() ) : the_post();
            if( has_term( $term, PL_PRODUCT_CATEGORIES_TAX_NAME ) ) :
              
              get_template_part('template-parts/archive/products/card-product');

            endif; 
          endwhile; ?>
        </div>

      </div>
    </section>

  <?php endif;
$i++; endforeach;
