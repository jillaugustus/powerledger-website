<?php

//=============================================================================================================
// Define constant variables
//=============================================================================================================

// Define constants
define( 'PL_PRODUCTS_CPT_NAME', 'product' );
define( 'PL_PRODUCTS_SINGLE_NAME', 'Product' );
define( 'PL_PRODUCTS_PLURAL_NAME', 'Products' );

// Define permalinks
$archive_page_id = get_option( 'page_for_' . PL_PRODUCTS_CPT_NAME );
$archive_page_slug = str_replace( home_url(), '', get_permalink( $archive_page_id ) );
$archive_permalink = ( $archive_page_id ? $archive_page_slug : 'products' );
$archive_permalink = ltrim( $archive_permalink, '/' );
$archive_permalink = rtrim( $archive_permalink, '/' );
define( 'PL_PRODUCTS_REWRITE_SLUG', $archive_permalink );




//=============================================================================================================
// Register CPT
//=============================================================================================================

function pl_products_register_cpt() {

	$labels = array(
		'name'                      => PL_PRODUCTS_PLURAL_NAME,
		'singular_name'             => PL_PRODUCTS_SINGLE_NAME,
		'menu_name'                 => PL_PRODUCTS_PLURAL_NAME,
		'name_admin_bar'            => PL_PRODUCTS_SINGLE_NAME,
		'archives'              		=> 'Product archives',
		'attributes'            		=> 'Product attributes',
		'parent_item_colon'     		=> 'Parent product:',
		'all_items'             		=> 'All products',
		'add_new_item'          		=> 'Add new product',
		'add_new'               		=> 'Add new product',
		'new_item'              		=> 'New product',
		'edit_item'             		=> 'Edit product',
		'update_item'           		=> 'Update product',
		'view_item'             		=> 'View product',
		'view_items'            		=> 'View products',
		'search_items'          		=> 'Search product',
		'not_found'             		=> 'Not found',
		'not_found_in_trash'    		=> 'Not found in Trash',
		'featured_image'        		=> 'Featured Image',
		'set_featured_image'    		=> 'Set featured image',
		'remove_featured_image' 		=> 'Remove featured image',
		'use_featured_image'    		=> 'Use as featured image',
		'insert_into_item'      		=> 'Insert into product',
		'uploaded_to_this_item' 		=> 'Uploaded to this product',
		'items_list'            		=> 'Products list',
		'items_list_navigation' 		=> 'Products list navigation',
		'filter_items_list'     		=> 'Filter products list'
	);

	$rewrite = array(
		'slug'                  		=> 'product', // The slug for single posts
		'with_front'            		=> false,
		'pages'                 		=> true,
		'feeds'                 		=> false,
	);

	$args = array(
		'label'                 		=> PL_PRODUCTS_SINGLE_NAME,
		'description'           		=> 'Content for a single Power Ledger product',
		'labels'                		=> $labels,
		'supports'              		=> array( 'title', 'thumbnail', 'revisions', 'page-attributes' ),
		'taxonomies'            		=> array(),
		'hierarchical'          		=> true,
		'public'                		=> true,
		'show_ui'               		=> true,
		'show_in_menu'          		=> true,
		'menu_position'         		=> 20,
		'menu_icon'             		=> 'dashicons-screenoptions',
		'show_in_admin_bar'     		=> true,
		'show_in_nav_menus'     		=> true,
		'can_export'            		=> true,
		'has_archive'           		=> PL_PRODUCTS_REWRITE_SLUG, // The slug for archive
		'exclude_from_search'   		=> false,
		'publicly_queryable'    		=> true,
		'capability_type'       		=> 'page',
		'rewrite'										=> $rewrite
	);
	register_post_type( PL_PRODUCTS_CPT_NAME, $args );

}
add_action( 'init', 'pl_products_register_cpt', 0 );




// Change CPT title placeholder on edit screen
function pl_products_cpt_title_placeholder( $title, $post ) {

	if ( $post->post_type == PL_PRODUCTS_CPT_NAME ) {
		return 'Add product title';
	}
	return $title;
}
add_filter( 'enter_title_here', 'pl_products_cpt_title_placeholder', 10, 2 );




// Change the number of 'posts per page' and order by 'menu order' for the CPT archive
function pl_products_cpt_modify_archive_query( $query ) {

	if ( $query->is_post_type_archive( PL_PRODUCTS_CPT_NAME ) && !is_admin() && $query->is_main_query() ) {
		
		$order_by = array( 
			'menu_order' => 'ASC',
			'date' => 'DESC'
		);

		$query->set( 'orderby', $order_by );
		$query->set( 'posts_per_page', '-1' );
	}

	return $query;
}
add_filter( 'pre_get_posts', 'pl_products_cpt_modify_archive_query' );




// Register a new column in admin list view for menu order
function pl_products_cpt_admin_table_column( $defaults ) {
	
	$new_order = array();

	foreach( $defaults as $key=>$value ) {
		if( $key=='date' ) {  // When we find the date column
			$new_order['menu_order'] = 'Order'; // Slip in the new column before it
		}
		$new_order[$key] = $value;
	}

	return $new_order;
}
add_filter( 'manage_' . PL_PRODUCTS_CPT_NAME . '_posts_columns', 'pl_products_cpt_admin_table_column' );




// Return the menu order in admin list view for each post
function pl_products_cpt_admin_table_content( $column_name, $post_id ) {
		
	global $post;
		
	if ( $column_name == 'menu_order' ) {
		$order = $post->menu_order;
		echo $order;
	}
}
add_action( 'manage_' . PL_PRODUCTS_CPT_NAME . '_posts_custom_column', 'pl_products_cpt_admin_table_content', 10, 2 );




// Make the admin menu order column sortable
function pl_products_cpt_admin_table_sortable( $columns ) {
	$columns['menu_order'] = 'menu_order';
	return $columns;
}
add_filter( 'manage_edit-' . PL_PRODUCTS_CPT_NAME . '_sortable_columns', 'pl_products_cpt_admin_table_sortable' );
