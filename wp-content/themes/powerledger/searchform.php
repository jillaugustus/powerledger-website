<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
  <label class="screen-reader-text" for="search">Type your search and press enter</label>
  <input class="search-input" id="site-search" name="s" placeholder="Enter search terms" type="search" value="<?php the_search_query(); ?>">
  <button type="submit" name="search-submit" class="search-submit">
    <span class="screen-reader-text">Submit your search request</span>
  </button>
</form>
