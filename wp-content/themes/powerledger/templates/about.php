<?php
/**
 * Template name: About Us
 * Template Post Type: page
 *
 * @package powerledger
 */

get_header(); 
  
  get_template_part('template-parts/page-header');

  get_template_part('template-parts/page/about/intro');

  get_template_part('template-parts/page/about/mission');

  get_template_part('template-parts/page/about/quote');

  get_template_part('template-parts/page/about/history');

  get_template_part('template-parts/page/about/team');

  get_template_part('template-parts/global-enquiry');

get_footer();
