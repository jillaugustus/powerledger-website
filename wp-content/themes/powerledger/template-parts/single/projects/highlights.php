<?php 

// Set empty array to store each highlight
$highlights_array = array();

// Set the max number of highlight items possible
$highlight_count = 4;

// Increment and loop through the count above and conditionally add to array if ACF fields are set
for($i = 1; $i<=$highlight_count; $i++) {

  if( get_field('highlight_' . $i . '_value') && get_field('highlight_' . $i . '_text') ) {
    $highlights_array[] = array(
      'value'     => get_field('highlight_' . $i . '_value'),
      'text'      => get_field('highlight_' . $i . '_text'),
    );
  }
}

// Check if the array is not still empty before displaying the section
if( !empty( $highlights_array ) ) : ?>

  <section class="section section-highlights bg-texture">
    <div class="container">

      <div class="section-header">
        <h2 class="section-title">Project highlights</h2>
      </div>

      <div class="highlight-items">

        <?php foreach ( $highlights_array as $key => $value ) : ?>
          
          <div class="highlight-item">
            <h3 class="title"><?php echo $value['value'] ?></h3>
            <p class="sub-title"><?php echo $value['text'] ?></p>
          </div>

        <?php endforeach; ?>
        
      </div>

    </div>
  </section>

<?php endif;
