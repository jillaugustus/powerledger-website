<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package powerledger
 */

get_header();

get_template_part('template-parts/page-header'); ?>

	<main class="posts-archive">
		<section class="section bg-device-left">
			<div class="container">

				<?php if ( have_posts() ) : ?>

					<div class="posts-list">

						<?php while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', 'post' );

						endwhile; ?>

					</div>

				<?php else :

					get_template_part( 'template-parts/content', 'none' );

				endif; 
				
				get_template_part( 'template-parts/pagination' ); 
				
				?>
				
			</div>
		</section>

	</main>

<?php

get_template_part('template-parts/global-enquiry'); 

get_footer();
