<?php
/**
 * Template name: Blockchain Technology
 * Template Post Type: page
 *
 * @package powerledger
 */

get_header();

  get_template_part('template-parts/page-header');

  get_template_part('template-parts/page/blockchain/intro');

  get_template_part('template-parts/page/blockchain/why');
  
  get_template_part('template-parts/page/blockchain/quote-1');

  get_template_part('template-parts/page/blockchain/benefits-1');

  get_template_part('template-parts/page/blockchain/benefits-2');

  get_template_part('template-parts/page/blockchain/quote-2');

  get_template_part('template-parts/page/blockchain/token-model');

get_footer();
