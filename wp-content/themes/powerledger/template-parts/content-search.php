<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package powerledger
 */

// Variables
$custom_title = get_field('page_custom_title');
$title = ($custom_title) ? $custom_title : get_the_title();
$sub_title = get_field('page_subtitle');

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<a class="entry-link" href="<?php echo the_permalink(); ?>" target="_self">
		<div class="entry-preview">

			<h2 class="entry-title h3"><?php echo $title ?></h2>

			<?php if( $sub_title ) : ?>
				<p class="entry-sub-title" aria-hidden="true">
					<?php echo $sub_title ?>
				</p>
			<?php endif; ?>

			<div class="entry-actions" aria-hidden="true">
				<span class="read-more">View <?php echo strtolower( get_post_type_object( get_post_type() )->labels->singular_name ); ?></span>
			</div>

		</div>
	</a>
</article>
