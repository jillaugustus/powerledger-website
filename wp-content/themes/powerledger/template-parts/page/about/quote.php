<?php 

// Variables
$author = get_field('testimonial_author');
$text = get_field('testimonial_text');

if($author && $text) : ?>

  <section class="section section-testimonial bg-texture">
    <div class="container">
      <div class="quote"><?php echo $text ?></div>
      <strong class="attribution"><?php echo $author ?></strong>
    </div>
  </section>

<?php endif;