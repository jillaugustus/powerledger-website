<?php

$social_links_array = array(
	'Facebook'      => 'https://www.facebook.com/powerledger',
	'Instagram'     => 'https://www.instagram.com/powerledger_io/',
	'Vimeo'         => 'https://vimeo.com/powerledgerio',
	'Telegram'      => 'https://t.me/powerledgerANN',
	'Twitter'       => 'https://twitter.com/PowerLedger_io',
	'YouTube'       => 'https://www.youtube.com/channel/UCB_IQppQmk08jWhVEBx0_jw',
	'LinkedIn'      => 'https://www.linkedin.com/company/power-ledger/'
);

if( !empty( $social_links_array ) ) : ?>

	<ul class="social-links">

		<?php foreach ( $social_links_array as $key => $value ) : 
			if( $value ) : ?>

				<li class="social-icon" >
					<a href="<?php echo esc_url( $value ) ?>" target="_blank" rel="noopener">
						<i class="icon <?php echo strtolower( $key ) ?>"></i>
						<span class="screen-reader-text"><?php echo $key ?></span>
					</a>
				</li>

			<?php endif; 
		endforeach; ?>
	</ul>

<?php endif;