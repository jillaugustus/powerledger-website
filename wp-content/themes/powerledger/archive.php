<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package powerledger
 */

get_header();

get_template_part('template-parts/page-header'); ?>

	<main class="<?php echo strtolower( get_post_type_object( get_post_type() )->labels->name ) . '-archive'; ?>">

		<?php
		
		// For certain post types, call in unique layout files that include extra sections
		// The loop is handled in each of these files respectively 
		if( is_post_type_archive( PL_PROJECTS_CPT_NAME ) ) : 

			get_template_part('templates/archive/projects');

		elseif( is_post_type_archive( PL_PRODUCTS_CPT_NAME ) ) : 

			get_template_part('templates/archive/products');

		elseif( is_post_type_archive( PL_JOBS_CPT_NAME ) ) : 

			get_template_part('templates/archive/jobs');

		// For everything else, create a standard archive layout with a post list
		else : ?>

			<section class="section bg-device-left">
				<div class="container">

					<?php if ( have_posts() ) : ?>

						<div class="posts-list">

							<?php while ( have_posts() ) : the_post();

								get_template_part( 'template-parts/content', 'post' );

							endwhile; ?>

						</div>

					<?php else :

						get_template_part( 'template-parts/content', 'none' );

					endif; 
					
					get_template_part( 'template-parts/pagination' ); 
					
					?>
					
				</div>
			</section>

		<?php endif; ?>

	</main>

<?php

get_template_part('template-parts/global-enquiry'); 

get_footer();
