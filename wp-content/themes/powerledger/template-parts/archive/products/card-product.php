<?php 

// Variables
$image = get_the_post_thumbnail_url( get_the_ID(), 'large' );
$custom_title = get_field('page_custom_title');
$title = ($custom_title) ? $custom_title : get_the_title();
$sub_title = get_field('page_subtitle');

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <a class="entry-link" href="<?php the_permalink(); ?>" target="_self">
    <div class="entry-inner">

      <div class="entry-thumb"<?php if( has_post_thumbnail() ) : ?> style="background-image: url('<?php echo $image; ?>');"<?php endif; ?>>
        <div class="ratio-sizer"></div>
      </div>

      <div class="entry-preview">

        <h2 class="entry-title h4"><?php echo $title ?></h2>

        <?php if($sub_title) : ?>
          <span class="entry-sub-title" aria-hidden="true"><?php echo $sub_title; ?></span>
        <?php endif; ?>

      </div>
    </div>
  </a>
</article>
