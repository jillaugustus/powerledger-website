jQuery(function ($) {

	var search_panel = '.search-panel',
		search_panel_trigger = '.search-panel-trigger',
		search_panel_input = '.search-panel .search-form input',
		mobile_menu = '.header .mobile-menu',
		mobile_menu_trigger = '.mobile-menu-trigger',
		desktop_menu = '.header .desktop-menu ul.main-menu',
		desktop_submenu_width = 300,
		sidebar_menu = '.sidebar-menu',
		body = 'body',
		timer; // Universal timer variable, must be set and reset whenever used

	// =======================================================================================================================
	// CALLBACK FUNCTIONS 
	// =======================================================================================================================

  // Disable scroll function
	function disableScroll() {
		
		if ( ($(body).hasClass('disable-scroll') == true) && (($(body).attr("data-offsettop") != undefined) == false ) )  {
			$(function() {
				var scrollPos = $(window).scrollTop();

				$(body).attr('data-offsettop', scrollPos).css('cssText', 'position: fixed; width: 100%; top: ' + -scrollPos + 'px !important;');
			})
		} else if ( ($(body).hasClass('disable-scroll') == false) && (($(body).attr('data-offsettop') != undefined) == true ) ) {
			$(function() {
				var scrollPos = $(body).attr('data-offsettop') ? $(body).attr('data-offsettop') : 0;

				$(body).removeAttr('data-offsettop').css('cssText', 'position: ; top: 0');
				$(window).scrollTop(scrollPos);
			})
		}
  }
  
	// Callback function to OPEN the desktop menu sub menu
	function openDesktopSubMenu(thisElement) {

    var element_left_position = $(thisElement).offset().left;
    var window_right_offset = $(window).width() + $(document).scrollLeft() - desktop_submenu_width;

    // If the distance between the menu item's left edge and the right side of screen is less than the submenu's width,
    // then align the submenu to the right instead, so the submenu doesn't overflow the page
    if( element_left_position > window_right_offset ) {
      $(thisElement).children('.sub-menu').addClass('align-right');
    }
    // Otherwise, remove this class and reset the position
    // We're resetting this in case the header's responsive formating changes the menu items left position
    // So basically, we want to check the menu items right offset every time it is hovered
    else {
      $(thisElement).children('.sub-menu').removeClass('align-right');
    }

    $(thisElement).addClass('hover').children('.sub-menu').stop(true).fadeIn(300);
	}

	// Callback function to CLOSE the desktop menu sub menu
	function closeDesktopSubMenu(thisElement) {
		$(thisElement).removeClass('hover').children('.sub-menu').fadeOut(200);
  }
  
  // Callback function to OPEN the overlay panel
	function openOverlayPanel() { 
    if($(body).hasClass('is-overlay') === false ) {
      $(body).addClass('is-overlay disable-scroll');
    }
  }
  
  // Callback function to CLOSE the overlay panel
	function closeOverlayPanel() { 
    if($(body).hasClass('is-overlay') === true ) {
      $(body).removeClass('is-overlay disable-scroll');
    }
	}

	// Callback function to OPEN the mobile menu
	function openMobileMenu() { 
		$(mobile_menu_trigger).addClass('open');
    $(body).addClass('is-mobile-menu');
	}

	// Callback function to CLOSE the mobile menu
	function closeMobileMenu() { 
		$(mobile_menu_trigger).removeClass('open')
    $(body).removeClass('is-mobile-menu');
    
		setTimeout(function(){
			$(mobile_menu).find('.menu-item-has-children').removeClass('sub-menu-open').find('.toggle-sub-menu .screen-reader-text').text('Expand sub menu');
			$(mobile_menu).find('.sub-menu').slideUp().attr("aria-hidden", "true");
		}, 500);
	}

	// Callback function to OPEN the search panel
	function openSearchPanel() { 
		$(search_panel_trigger).addClass('open');
		$(search_panel_trigger).find('.label-swapper').addClass('swap');
    $(body).addClass('is-search');

		setTimeout(function(){ // Short delay to wait until transitions finish before we focus on the input
			$(search_panel_input).focus();
		}, 100);
	}

	// Callback function to CLOSE the search panel
	function closeSearchPanel() { 
		$(search_panel_trigger).removeClass('open');
		$(search_panel_trigger).find('.label-swapper').removeClass('swap');
		$(body).removeClass('is-search');
    $(search_panel_input).blur();
	}

	// Callback function to OPEN AND CLOSE the dropdown menus for mobile menu
	function toggleDropdownMenu(thisElement) { 

		var parent_item = '.menu-item-has-children';
		var sub_menu = '.sub-menu';

		// If this item's sub menu is currently hidden
		if ($(thisElement).siblings(sub_menu).is(':hidden') === true) {
			
			// Change this items screen reader text
			$(thisElement).children('.screen-reader-text').text('Collapse sub menu');

			// Add class to this item and open its direct sub menu
			$(thisElement).parent(parent_item).addClass('sub-menu-open').children(sub_menu).slideDown().attr("aria-hidden", "false");

			// If any neighboring items sub menu is open
			if ($(thisElement).parent(parent_item).siblings().hasClass('sub-menu-open') === true) {

				$(thisElement).parent(parent_item).siblings().each(function(){

					// Then remove the class from that item, and search for any children that are open and remove the class there and reset their button's screen reader text as well
					$(this).removeClass('sub-menu-open').children('.toggle-sub-menu').find('.screen-reader-text').text('Expand sub menu');
					$(this).find(parent_item).removeClass('sub-menu-open').children('.toggle-sub-menu').find('.screen-reader-text').text('Expand sub menu');

					// Find any open neighboring items, collapse the sub menu, and then search through each of them for any children that are open and collapse those sub menus too
					$(this).find(sub_menu).slideUp().attr("aria-hidden", "true");

				});
			}
		}
		
		// If this item's sub menu is currently visible
		else if ($(thisElement).siblings(sub_menu).is(':hidden') === false) {

			// Reset screen reader text for this item
			$(thisElement).children('.screen-reader-text').text('Expand sub menu');

			// Remove class from this item, then search for any children that are open and remove the class there too and reset their button's screen reader text
			$(thisElement).parent(parent_item).removeClass('sub-menu-open').find(parent_item).removeClass('sub-menu-open').find('.toggle-sub-menu .screen-reader-text').text('Expand sub menu');

			// Find any open sub menus within the current item and collapse them all
			$(thisElement).parent(parent_item).find(sub_menu).slideUp().attr("aria-hidden", "true");
		}
	}

	// Callback function to temporarily disable CSS transitions on selected objects for 1 millisecond while their state is changing
	// Primarily used to prevent elements animating on page load
	function disableTransitions() {

		var elementsToDisable = $([
			mobile_menu, 
			mobile_menu_trigger,
			mobile_menu_trigger + ' .hamburger',
			mobile_menu_trigger + ' .hamburger > div', 
			search_panel,
			search_panel_trigger + ' .icon',
			sidebar_menu + ' .sub-menu-open .icon',
			'.page-wrapper',
			'.header .header-bar'
		]);

		$.each(elementsToDisable, function(_index, value){

			$(value).addClass('disable-transitions'); // Disable the CSS transitions

			setTimeout(function(){
				$(value).removeClass('disable-transitions'); // Reactivate the CSS transitions after 0.1 seconds
			}, 100);
		});
	}

	// =======================================================================================================================
	// General behaviour functions 
	// =======================================================================================================================

	// Desktop submenu show/hide on hover
	$(desktop_menu + ' li.menu-item-has-children').hover(function() {
	
		var $this = this;

		timer = setTimeout(function(){
			openDesktopSubMenu($this);
		}, 100);

	}, function() {
		clearTimeout(timer);
		closeDesktopSubMenu(this);
	});

	// Desktop submenu show/hide with tabs
	$(desktop_menu + ' > li.menu-item-has-children a').focus(function() {
		$(this).closest('li.menu-item-has-children').each(function() {
			openDesktopSubMenu(this);
		});
	}).blur(function() { // If user tabs away from either the parent or one of the submenu links, then close the submenu

		$this = $(this); // Store 'this' as a variable so we can pass it through timeout

		setTimeout(function(){ // Add a short delay so the blur and focus functions don't fire simultaneously
			$this.closest('li.menu-item-has-children').each(function() {
				if ($(this).find(':focus').length == 0) { // Check if any child element has focus
					closeDesktopSubMenu(this); // If not, then run the callback to close the sub menu
				}
			});
		}, 10);
	});

	// Mobile menu show/hide on trigger click
	$(mobile_menu_trigger).on('click touch', function(e) {
		
		e.preventDefault();
		
		if ( $(this).hasClass('open') === false ) {
      openMobileMenu();
      openOverlayPanel();

			if( $(search_panel_trigger).hasClass('open') === true ) {
				closeSearchPanel();
			}
		} else if ( $(this).hasClass('open') === true ) {
      closeMobileMenu();
      closeOverlayPanel();
		}
		disableScroll();
	});
	
	// Mobile sub menu show/hide on toggle click
	$(mobile_menu).ready(function () {

		var parent_item = '.menu-item-has-children';
		var sub_menu = '.sub-menu';

		// Hide the sub menus and assign aria attributes
		$(mobile_menu).find(parent_item).children(sub_menu).hide().attr("aria-hidden", "true");

		// Add the aria attributes and toggle button to any menu items that have children
		$(mobile_menu).find(parent_item).each(function(){
			$(this).children('a').attr("aria-haspopup", "true").after("<button class='toggle-sub-menu'><i class='icon'></i><span class='screen-reader-text'>Expand sub menu</span></button>");
		});

		$(mobile_menu + ' .toggle-sub-menu').each(function() {

			$(this).on('click', function() {
				toggleDropdownMenu(this);
			});
		});
	});

	// Search panel show/hide on trigger click
	$(search_panel_trigger).on('click touchstart', function(e) {

		e.preventDefault();

		if ( $(this).hasClass('open') === false ) {
      openSearchPanel();
      openOverlayPanel();

			if( $(mobile_menu_trigger).hasClass('open') === true ) {
				closeMobileMenu();
			}

		} else if ( $(this).hasClass('open') === true ) {
      closeSearchPanel();
      closeOverlayPanel();
		}
		disableScroll();
	});

	// Sidebar sub menu show/hide on toggle click
	$(sidebar_menu).ready(function() {

		var parent_item = '.menu-item-has-children',
			sub_menu = '.sub-menu';

		// Hide the sub menus
		$(sidebar_menu).find(parent_item).children(sub_menu).hide().attr("aria-hidden", "true");

		// Expand the sub menu if user is currently on a sub page when the page loads
		$(sidebar_menu + ' li').each(function(){
			if ( $(this).hasClass('current_page_ancestor') || $(this).hasClass('current_page_item') && $(this).hasClass('page_item_has_children') ) {
				$(this).addClass('sub-menu-open').children(sub_menu).show();
				disableTransitions();
			}
		});

		$(sidebar_menu + ' .toggle-sub-menu').each(function () {

			$(this).on('click', function() {
				toggleDropdownMenu(this);
			});
		});
	});

	// If user has mobile menu open and resizes the browser to desktop size, then hide it and reenable scrolling
	$(window).on('resize', function() {

		if ( ( $(window).width() > 1024 ) && ( $(mobile_menu_trigger).hasClass('open') === true ) ) {
      closeMobileMenu();
      closeOverlayPanel();
			disableScroll();
		}
	});

	// Close search box if user clicks anwyhere outside of it or the the search trigger button
	// $(document).on('click touchstart', function (e) {

	// 	e.stopPropagation();

	// 	var target = $(e.target);

	// 	if( target.closest(search_panel).length || target.closest(search_panel_trigger).length ) {
	// 		return;
	// 	} else {
	// 		if ($(search_panel_trigger).hasClass('open')){
  //       closeSearchPanel();
  //       closeOverlayPanel();
	// 		}
	// 	}
	// });

	// Close search box if user presses esc key while it is open
	$(document).on('keydown', function(e) {
		if ($(body).hasClass('is-search') && e.key == "Escape") {
      closeSearchPanel();
      closeOverlayPanel();
      disableScroll();
		}
		if ($(body).hasClass('is-mobile-menu') && e.key == "Escape") {
      closeMobileMenu();
      closeOverlayPanel();
			disableScroll();
		}
	});

	// Ensure that the search panel or the mobile menu do no show if the user uses the back/forwards arrows
	// Mainly concerns safari/firefox which cache pages
	// This is the main purpose of the disableTransitions() function 
	$(window).bind('pageshow', function(event) {  

		if (event.originalEvent.persisted) {
			resizeselect();
		
			if( $(mobile_menu_trigger).hasClass('open') === true ) {
				disableTransitions();
        closeMobileMenu();
        closeOverlayPanel();
			}

			if( $(search_panel_trigger).hasClass('open') === true ) {
				disableTransitions();
        closeSearchPanel();
        closeOverlayPanel();
			}

			disableScroll();
		}
  });



  
  // ======================================================================================================================
	// Other funtions
	// =======================================================================================================================

	// Resize select fields to match their chosen options width	
	function resizeselect() { 

		var arrowWidth = 20; // Offset for arrow
	
		$('.resize-select').each(function() {

			var $this = $(this);
	
			var text = $this.find("option:selected").text();
			var $selected = $("<span>").html(text).addClass('select-styles');
	
			$selected.appendTo('body');
			var width = $selected.width();
			$selected.remove();
		
			$this.width(width+arrowWidth); // Combine the widths

		});
	}	
	resizeselect(); // Run the function on page load
	$("select.resize-select").change(resizeselect); // Run the function whenever the select field is changed

	// Skip to buttons
	$('a.skip-to').each(function() { 

		$(this).on('click', function(e) { 

			if ( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname ) {

				// Find the target to scroll to
				var target = $(this.hash);

				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

				// Check that a scroll target exists
				if (target.length) {

					// Unfocus the skip to button
					$(this).blur();

					// Only prevent default if animation is going happen
					e.preventDefault();

					// Set variables
					var target_top = target.offset().top;
					var header_height = $('.header .header-bar').outerHeight() + 100; // Add some extra pixels so the target always lands below the header
					var offset = target_top - header_height;

					// Now run the scroll animation
					$('html, body').animate({ scrollTop: offset }, 500, function() {

						// Add tabindex incase the target isn't a focusable element, and then focus 
						$(target).attr('tabindex', -1).on('blur focusout', function () {
							$(this).removeAttr('tabindex'); // when focus leaves the target element, remove the tabindex attribute
						}).focus();
					});
				}
			}
		});
	});

	// Remove empty tags
	$('p:empty').remove();
	
	// Stop word making things ugly
  $('span').removeAttr('style');
  
  // Overlay modaals
  $('.modaal-trigger').modaal({
    overlay_opacity: 0.9,
  });

	// Modaal video overlay setup
	$('.video-overlay').modaal({
		type: 'video'
  });
  
  // Validate number inputs if a min or max attribute is set
  $('input[type=number]').each(function(){

    var max = parseInt($(this).attr('max'));
    var min = parseInt($(this).attr('min'));

    $(this).change(function() {

      if ( typeof max !== 'undefined' && $(this).val() > max) {
        $(this).val(max);
      }
      else if ( typeof min !== 'undefined' && $(this).val() < min) {
        $(this).val(min);
      }
      
    });
  });

});





// =======================================================================================================================
// TO BE PHASED OUT
// =======================================================================================================================

jQuery(document).ready(function($) {

  // sticky nav state toggle
  $(document).on('scroll', function() {
    var distanceScrolled = $(window).scrollTop();

    if(distanceScrolled >= 20) {
      $('html').addClass('scrolled');
    }
    else {
      $('html').removeClass('scrolled');
    }
  });

  // Remove cta bar at certain scroll
  if($('body').hasClass('home') && $('.footer-subscribe').length) {
    $(document).on('scroll', function() {
      var distanceScrolled = $(window).scrollTop();
      var targetDistanceFromTop = $('.footer-subscribe').offset().top;
      var windowHeight = $(window).height();
      var triggerDistance = targetDistanceFromTop - windowHeight;

        if(distanceScrolled >= triggerDistance) {
          $('html').addClass('footer-in-view');
        }
        else {
          $('html').removeClass('footer-in-view');
        }
    });
  }

  // slick slider activation
  $('.carousel-wrapper').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true,
    autoplaySpeed: 5000,
    infinite: false,
    dots: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          arrows: true,
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false
        }
      }
    ]
  });

  // linkjacking inactive hrefs in slick slider, which seems to strip away any normal link behaviour
  $(document).on('click', '.slick-slide', function(event) {
    event.preventDefault();
    var url = $(this).find('a').attr('href');
    if($(this).find('a').attr('target') == '_blank') {
      window.open(url, '_blank');
    }
    else {
      window.location.href = url;
    }
  });


  //smooth scroll
  var scroll = new SmoothScroll('a.smooth', {
    header: 'header'
  });

  // VPP form scroll repositioning
  if($('body').hasClass('page-template-page-vpp-registration')) {
    var sectionHero = $('.section-landing-hero');
    var sectionFeatures = $('.section-landing-features');

    // defining start and end points for the form's movement 'track', based on
    // the mid point of the hero section and the mid point of the features section
    var movementStart = sectionHero.offset().top + sectionHero.outerHeight() / 2;
    var movementEnd = sectionFeatures.offset().top + sectionFeatures.outerHeight() / 2;

    // initialising the form position and setting it to move on scroll
    $('.form').css('top', movementStart + 'px');

    $(document).on('scroll', function () {
      // on scroll, watch form position
      var formPosition = $(document).scrollTop() + movementStart;

      // when form position has exceeded end of track, manually set css 'top' value
      // based on scroll distance, to force a faux 'stuck' effect
      if (formPosition >= movementEnd) {
        $('.form').css('top', $(document).scrollTop() * -1 + movementEnd);
      } else {
        $('.form').css('top', movementStart + 'px');
      }
    });

    // if window is resized, recalibrate the track start and end given potentially
    // new section height values
    $(window).on('resize', function() {
      movementStart = sectionHero.offset().top + sectionHero.outerHeight() / 2;
      movementEnd = sectionFeatures.offset().top + sectionFeatures.outerHeight() / 2;
    });
  }

  // Projects page interactive map
  if($('.post-type-archive-project').length) {
    $('#continents > g').on('click', function() {
      var continent = $(this).attr('id');
      $('#continents > g').removeClass('clicked');
      $(this).addClass('clicked');
      $('.' + continent).addClass('continent-active');
    });

    $('.project-continent button').on('click', function() {
      $('.project-continent').removeClass('continent-active');
      $('#continents > g').removeClass('clicked');
    });
  }
});
