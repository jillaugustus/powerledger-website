<?php

$args = array(
  'post_type' => 'Thoughts',
  'nopaging' => true
);
$thoughts_loop = new WP_Query($args); ?>

  <?php if( $thoughts_loop -> have_posts() ): ?>
    <section class="section-carousel">
      
      <div class="container">
        <div class="section-header">
          <h2 class="section-title">Thought leadership</h2>
          <span class="section-sub-title">Our co-founders write regularly for a number of top-tier publications including Forbes and Entrepreneur APAC. See some recent articles below:</span>
        </div>
      </div>

      <div class="carousel-wrapper">
        <?php while( $thoughts_loop -> have_posts() ) : $thoughts_loop -> the_post(); ?>
          <a href="<?php the_field('article_link'); ?>" aria-label="<?php the_field('article_link'); ?>" target ="_blank">
            <article>
              <div class="article-image" style="background-image: url('<?php echo wp_get_attachment_image_src( get_field('hero_image'), "article" )[0]; ?>');"></div>
              <h3><?php the_field('hero_title'); ?></h3>
              <p><?php the_field('author'); ?></p>
              <p><?php the_field('subtitle', false, false); ?></p>
              <span>Read more</span>
            </article>
          </a>
        <?php endwhile; wp_reset_postdata(); ?>
      </div>
    </section>
  <?php endif;
