<?php

//=============================================================================================================
// Define constant variables
//=============================================================================================================

// Define constants
define( 'PL_THOUGHT_LEADERSHIP_CPT_NAME', 'thoughts' );
define( 'PL_THOUGHT_LEADERSHIP_SINGLE_NAME', 'Thought' );
define( 'PL_THOUGHT_LEADERSHIP_PLURAL_NAME', 'Thoughts' );

// Define permalinks
define( 'PL_THOUGHT_LEADERSHIP_REWRITE_SLUG', 'thought-leadership' );




//=============================================================================================================
// Register CPT
//=============================================================================================================

function pl_thought_leadership_register_cpt() {

	$labels = array(
		'name'                      => PL_THOUGHT_LEADERSHIP_PLURAL_NAME,
		'singular_name'             => PL_THOUGHT_LEADERSHIP_SINGLE_NAME,
		'menu_name'                 => PL_THOUGHT_LEADERSHIP_PLURAL_NAME,
		'name_admin_bar'            => PL_THOUGHT_LEADERSHIP_SINGLE_NAME,
		'archives'              		=> 'Thought Leadership archives',
		'attributes'            		=> 'Thought Leadership attributes',
		'parent_item_colon'     		=> 'Parent article:',
		'all_items'             		=> 'All articles',
		'add_new_item'          		=> 'Add new article',
		'add_new'               		=> 'Add new article',
		'new_item'              		=> 'New article',
		'edit_item'             		=> 'Edit article',
		'update_item'           		=> 'Update article',
		'view_item'             		=> 'View article',
		'view_items'            		=> 'View articles',
		'search_items'          		=> 'Search article',
		'not_found'             		=> 'Not found',
		'not_found_in_trash'    		=> 'Not found in trash',
		'featured_image'        		=> 'Featured Image',
		'set_featured_image'    		=> 'Set featured image',
		'remove_featured_image' 		=> 'Remove featured image',
		'use_featured_image'    		=> 'Use as featured image',
		'insert_into_item'      		=> 'Insert into article',
		'uploaded_to_this_item' 		=> 'Uploaded to this article',
		'items_list'            		=> 'Articles list',
		'items_list_navigation' 		=> 'Articles list navigation',
		'filter_items_list'     		=> 'Filter articles list'
	);

	$rewrite = array(
		'slug'                  		=> PL_THOUGHT_LEADERSHIP_REWRITE_SLUG,
		'with_front'            		=> false,
		'pages'                 		=> true,
		'feeds'                 		=> false
	);

	$args = array(
		'label'                 		=> PL_THOUGHT_LEADERSHIP_SINGLE_NAME,
		'description'           		=> 'Content for thought leadership articles',
		'labels'                		=> $labels,
		'supports'              		=> array( 'title', 'editor', 'thumbnail', 'revisions', 'author' ),
		'taxonomies'            		=> array(),
		'hierarchical'          		=> false,
		'public'                		=> true,
		'show_ui'               		=> true,
		'show_in_menu'          		=> true,
		'menu_position'         		=> 20,
		'menu_icon'             		=> 'dashicons-welcome-write-blog',
		'show_in_admin_bar'     		=> true,
		'show_in_nav_menus'     		=> true,
		'can_export'            		=> true,
		'has_archive'           		=> false,
		'exclude_from_search'   		=> false,
		'publicly_queryable'    		=> false,
		'capability_type'       		=> 'page',
		'rewrite'										=> $rewrite
	);
	register_post_type( PL_THOUGHT_LEADERSHIP_CPT_NAME, $args );

}
add_action( 'init', 'pl_thought_leadership_register_cpt', 0 );




// Change CPT title placeholder on edit screen
function pl_thought_leadership_cpt_title_placeholder( $title, $post ) {

	if ( $post->post_type == PL_THOUGHT_LEADERSHIP_CPT_NAME ) {
		return 'Add article title';
	}
	return $title;
}
add_filter( 'enter_title_here', 'pl_thought_leadership_cpt_title_placeholder', 10, 2 );
