<?php 

// Variables
$image = wp_get_attachment_image_src( get_field('how_we_started_image'), 'large' )[0];
$title = get_field('how_we_started_title');
$text = get_field('how_we_started_text');

?>

<section class="section section-about-why bg-device-left">
  <div class="container">
    <div class="section-column section-content">
      <div class="content-wrap">

        <h2><?php echo $title ?></h2>

        <?php if($text) : echo $text; endif; ?>

      </div>
    </div>

    <div class="section-column section-image-cover" style="background-image: url('<?php echo $image ?>');"></div>
  </div>
</section>