<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package powerledger
 */

// Variables
$topic_terms = get_terms( PL_ANNOUNCEMENT_TOPICS_TAX_NAME );

if( ( PL_ANNOUNCEMENTS_CPT_NAME === get_post_type() && function_exists('naked_social_share_buttons') ) || PL_ARTICLES_CPT_NAME === get_post_type() ) : ?>

	<aside class="post-aside">
		
		<?php if( function_exists('naked_social_share_buttons') ) : ?>

			<h2>Share</h2>
			<?php naked_social_share_buttons(); ?>

		<?php endif; ?>

		<?php if( PL_ANNOUNCEMENTS_CPT_NAME === get_post_type() ) :

			if( $topic_terms ) : ?>
			
				<h2>Topics</h2>
				<div class="topic-entries">
					<?php foreach( $topic_terms as $term ) { ?>
						<a href="<?php echo esc_url( get_category_link( $term->term_id ) ); ?>"><?php echo esc_html( $term->name ); ?></a>
					<?php } ?>
				</div>

			<?php endif;
		endif; ?>
	</aside>

<?php else :

	if ( is_active_sidebar( 'sidebar-1' ) ) : ?>

		<aside id="secondary" class="widget-area">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</aside>

	<?php endif;

endif;
