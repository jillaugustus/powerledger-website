<div id="vpp-registration-form" class="modaal-content-source">
  <h2 class="h3">VPP registration of interest</h2>
  <!-- Embedding hubspot contact form -->
  <!--[if lte IE 8]>
  <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
  <![endif]-->
  <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
  <script>
    hbspt.forms.create({
      portalId: "4519667",
      formId: "c9133c9a-83f8-4e5e-b389-f0a08d519bb1"
    });
  </script>
</div>
