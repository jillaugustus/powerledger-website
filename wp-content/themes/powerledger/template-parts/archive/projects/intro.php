<?php 

// Variables
$archive_page_id = get_post_type_archive_page_id();

$title = get_field('projects_archive_intro_title', $archive_page_id);
$text = get_field('projects_archive_intro_text', $archive_page_id);

?>

<section class="section-projects-overview bg-texture">
  <div class="container">

    <div class="section-column">
      <h2><?php echo $title ?></h2>
      <?php echo $text ?>
    </div>

    <div class="section-column projects-map">

      <?php include get_template_directory() . '/images/world-map-continents.svg';
      
      $continents = get_terms([
        'taxonomy'          => 'continent',
        'hide_empty'        => false,
      ]);

      foreach( $continents as $continent ) :

        $projects_loop = new WP_Query( array(
          'post_type'         => PL_PROJECTS_CPT_NAME,
          'posts_per_page'    => 15, // Important for a PHP memory limit warning
          'tax_query'         => array(
            array(
              'taxonomy'        => 'continent',
              'field'           => 'slug',
              'terms'           => $continent->slug,
            ),
          ),
        ));

        ?>

        <div class='project-continent <?php echo $continent->slug ?>'>
          <div class='content'>

            <h3>Projects in <?php echo $continent->name ?></h3>
    
            <?php if( $projects_loop->have_posts() ) :
    
              while ( $projects_loop->have_posts() ) : $projects_loop->the_post(); ?>
          
                <a href="<?php the_permalink(); ?>" class="project-link" aria-label="<?php the_field('project_title'); ?>"><?php the_field('project_title'); ?></a>

              <?php endwhile; wp_reset_postdata();

            else: ?>
            
              <p>There are currently no active Power Ledger projects in this region. <a href="#enquiry-form-general" class="modaal-trigger">Partner with us</a></p>

            <?php endif; ?>

            <button><span>Close</span></button>
            
          </div>
        </div>

      <?php endforeach; ?>

    </div>
  </div>
</section>