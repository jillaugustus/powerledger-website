<?php 

// Variables
$image = wp_get_attachment_image_src( get_field('what_is_it_image'), 'large' )[0];
$video = get_field('what_is_it_video');
$title = get_field('what_is_it_title');
$text = get_field('what_is_it_text');
$button = get_field('what_is_it_button');

if( $text & $title ) : ?>

  <section class="section section-product-overview bg-device-left reverse pb0">
    <div class="container">

      <?php if( $video ) : ?>

        <div class="section-column section-video">
          <?php echo $video ?>
        </div>

      <?php elseif( $image ) : ?>

        <div class="section-column section-image-cover" style="background-image: url('<?php echo $image; ?>');"></div>

      <?php endif; ?>

      <div class="section-column section-content">
        <div class="content-wrap">

          <h2><?php echo $title ?></h2>
          <?php echo $text ?>
          
          <?php if( $button ) : ?>
              
            <div class="actions">
              <a href="<?php echo $button['url'] ?>" class="button"<?php if( $button['target'] ) : ?> target="blank" rel="noopener"<?php endif; ?>><?php echo $button['title'] ?></a>
            </div>

          <?php endif; ?>

        </div>
      </div>

    </div>
  </section>

<?php endif;
