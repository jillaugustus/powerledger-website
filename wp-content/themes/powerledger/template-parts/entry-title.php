<?php

// Set variable for page id, check first if the page is a dummy archive
if( is_archive() ) {
  $page_id = get_post_type_archive_page_id();
} else {
  $page_id = get_the_ID();
}

// Custom page titles
$custom_title = get_field('page_custom_title', $page_id);

// Index
if ( is_home() || is_front_page() ) :
	$title = get_the_title( get_option('page_for_posts', true) ); 

// Archive taxonomy pages
elseif ( is_tax() ) :
	$title = get_the_archive_title();

// Search result pages
elseif ( is_search() ) :
	global $wp_query;
	$result_terminology = $wp_query->found_posts > 1 ? ' results' : ' result'; // Change the terminology depending on whether there is 1 or more results
	$title = $wp_query->found_posts . $result_terminology . ' for: ' . get_search_query();

// Custom page titles
elseif ( $custom_title ) :
	$title = $custom_title;

// Regular title
else : 
	$title = get_the_title($page_id);
		
endif;

?>

<h1 class="entry-title<?php echo (strlen($title) < 55) ? ' big' : ''; ?>">
	<?php echo $title ?>
</h1>
