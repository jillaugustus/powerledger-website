<?php

get_template_part('template-parts/single/projects/intro'); 

get_template_part('template-parts/single/projects/quote');

get_template_part('template-parts/single/projects/highlights');

get_template_part('template-parts/project-carousel');

get_template_part('template-parts/global-enquiry');
