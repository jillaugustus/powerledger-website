<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package powerledger
 */

if( !is_single() ) :

	// Variables
	$image = get_the_post_thumbnail_url( get_the_ID(), 'large' );
	$custom_title = get_field('page_custom_title');
	$title = ($custom_title) ? $custom_title : get_the_title();
	$sub_title = get_field('page_subtitle');
	
	?>		

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<a class="entry-link" href="<?php the_permalink(); ?>" target="_self">
			<div class="entry-inner">

				<div class="entry-thumb"<?php if( has_post_thumbnail() ) : ?> style="background-image: url('<?php echo $image; ?>');"<?php endif; ?>>
					<div class="ratio-sizer"></div>
				</div>

				<div class="entry-preview">

					<div class="entry-meta" aria-hidden="true">
						<span class="posted-on"><?php echo get_the_date(); ?></span>
					</div>
					
					<h2 class="entry-title h4"><?php echo $title ?></h2>
					
					<?php if($sub_title) : ?>
						<span class="entry-sub-title" aria-hidden="true"><?php echo $sub_title; ?></span>
					<?php endif; ?>


				</div>
			</div>
		</a>
	</article>

<?php else : ?>

	<div class="container main-wrapper">

		<div class="entry-meta" aria-hidden="true">
			<span class="posted-on"><?php echo get_the_date(); ?></span>
		</div>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="article-wrap">
				<div class="entry-content">
					<div class="entry-content-wrap">

						<?php
							the_content();

							if(get_field('post_video')) :
								echo the_field('post_video');
							endif; 
						?>

					</div>
				</div>
			</div>
		</article>

		<?php get_sidebar(); ?>

	</div>

<?php endif;