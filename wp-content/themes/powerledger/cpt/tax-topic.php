<?php

//=============================================================================================================
// Define constant variables
//=============================================================================================================

define( 'PL_ANNOUNCEMENT_TOPICS_TAX_NAME', 'announcement-topic' );




//=============================================================================================================
// Register taxonomy
//=============================================================================================================

function pl_announcement_topics_register_taxonomy() {

	$labels = array(
		'name'                       => 'Announcement topics',
		'singular_name'              => 'Announcement topic',
		'menu_name'                  => 'Announcement topics',
		'all_items'                  => 'All Topics',
		'parent_item'                => 'Parent Topic',
		'parent_item_colon'          => 'Parent Topic:',
		'new_item_name'              => 'New Topic Name',
		'add_new_item'               => 'Add New Topic',
		'edit_item'                  => 'Edit Topic',
		'update_item'                => 'Update Topic',
		'view_item'                  => 'View Topic',
		'separate_items_with_commas' => 'Separate Topics with commas',
		'add_or_remove_items'        => 'Add or remove Topics',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular Topics',
		'search_items'               => 'Search Topics',
		'not_found'                  => 'Not Found',
		'no_terms'                   => 'No Topics',
		'items_list'                 => 'Topics list',
		'items_list_navigation'      => 'Topics list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( PL_ANNOUNCEMENT_TOPICS_TAX_NAME, PL_ANNOUNCEMENTS_CPT_NAME, $args );

}
add_action( 'init', 'pl_announcement_topics_register_taxonomy', 0 );
