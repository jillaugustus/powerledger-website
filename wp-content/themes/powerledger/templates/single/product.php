<?php

// Variables
$coming_soon = get_field('product_coming_soon');
$product_project_filter = get_field('product_project_filter');

if( $coming_soon ) :

  get_template_part('template-parts/single/products/coming-soon');

elseif( 506 === get_the_ID() ) : 

  get_template_part('template-parts/single/products/vpp-sa');

else :

  get_template_part('template-parts/single/products/intro');

  get_template_part('template-parts/single/products/how');

  get_template_part('template-parts/single/products/benefits');

  if( $product_project_filter ) : 
    get_template_part('template-parts/project-carousel');
  endif;

  get_template_part('template-parts/single/products/requirements');

  get_template_part('template-parts/single/products/enquire');

endif;
