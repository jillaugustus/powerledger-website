<?php
/**
 * Template name: Our Values
 * Template Post Type: page
 * 
 * @package powerledger
 */

get_header();

get_template_part('template-parts/page-header');
  
get_template_part('template-parts/page/values/intro');

get_template_part('template-parts/page/values/values-list');

get_template_part('template-parts/page/values/quote-1');

get_template_part('template-parts/page/values/proposition');

get_template_part('template-parts/page/values/quote-2');

get_template_part('template-parts/page/values/thoughts-carousel');

get_footer();
