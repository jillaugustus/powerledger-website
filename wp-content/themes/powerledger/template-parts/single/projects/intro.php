<?php 

// Variables
$image = wp_get_attachment_image_src( get_field('how_it_works_image'), 'large' )[0];
$video = get_field('how_it_works_video');
$summary = get_field('summary');
$how_title = get_field('how_it_works_title');
$how_text = get_field('how_it_works_content');
$button = get_field('project_summary_button');

if( $summary ) : ?>

  <section class="section section-project-overview bg-device-left">

    <div class="container">
      <div class="section-column">
        
        <div class="summary">

          <h2>Project summary</h2>
          <?php echo $summary ?>

        </div>

        <?php if( $how_title ) : ?>

          <div class="how">
            <h2><?php echo $how_title ?></h2>
            <?php echo $how_text ?>
          </div>

        <?php endif; ?>
        
        <?php if( $button ) : ?>
          
          <div class="actions">
            <a href="<?php echo $button['url'] ?>" class="button"<?php if( $button['target'] ) : ?> target="blank" rel="noopener"<?php endif; ?>><?php echo $button['title'] ?></a>
          </div>

        <?php endif; ?>

      </div>

      <?php

      ?>

      <?php if( $video ) : ?>

        <div class="section-column section-video">
          <?php echo $video ?>
        </div>

      <?php elseif( $image ) : ?>

        <div class="section-column section-image-cover" style="background-image: url('<?php echo $image; ?>');"></div>

      <?php endif; ?>

    </div>

  </section>

<?php endif;
