<?php 

// Variables
$benefit_1_title = get_field('benefits_one_title');
$benefit_1_text = get_field('benefits_one');
$benefit_2_title = get_field('benefits_two_title');
$benefit_2_text = get_field('benefits_two');

if ( $benefit_1_title && $benefit_1_text ) : ?>

  <section class="section section-features bg-texture">
    <div class="container">

      <div class="section-column section-content">
        <div class="content-wrap">
          <div class="entry-content">
            <div class="entry-content-wrap">
              <h2><?php echo $benefit_1_title ?></h2>
              <?php echo $benefit_1_text ?>
            </div>
          </div>
        </div>
      </div>

      <?php if ( $benefit_2_title && $benefit_2_text ) : ?>

        <div class="section-column section-content">
          <div class="content-wrap">
            <div class="entry-content">
              <div class="entry-content-wrap">
                <h2><?php echo $benefit_2_title ?></h2>
                <?php echo $benefit_2_text ?>
              </div>
            </div>
          </div>
        </div>

      <?php endif; ?>
      
    </div>  
  </section>

<?php endif;
