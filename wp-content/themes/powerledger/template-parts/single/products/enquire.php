<?php

// Variables
$text = get_field('call_to_action');

?>

<section class="section-enquiry bg-device-right">
    <div class="container">
      <h2>Register your interest</h2>
      <?php if($text) : echo $text; endif; ?>
      <a href="#enquiry-form-general" class="button modaal-trigger">Enquire about <?php the_title(); ?></a>
    </div>
</section>