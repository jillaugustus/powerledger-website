<?php 

// Variables
$image = wp_get_attachment_image_src( get_field('about_image'), 'large' )[0];
$title = get_field('about_title');
$text = get_field('about_text');
$link = get_field('whitepaper_link');

?>

<section class="section section-about-intro bg-device-left pb0">
  <div class="container">
    <div class="section-column section-content">
      <div class="content-wrap">

        <h2><?php echo $title ?></h2>

        <?php if($text) : echo $text; endif; ?>
        
        <?php if($link): ?>
          <a href="<?php echo $link ?>" target="_self" class="button">Read our whitepaper</a>
        <?php endif; ?>

      </div>
    </div>

    <div class="section-column section-image-cover" style="background-image: url('<?php echo $image ?>');"></div>
  </div>
</section>