<?php

//=============================================================================================================
// Define constant variables
//=============================================================================================================

define( 'PL_PROJECTS_PRODUCTS_TAX_NAME', 'project-products' );




//=============================================================================================================
// Register taxonomy
//=============================================================================================================

function pl_project_products_register_taxonomy() {

	$labels = array(
		'name'                       => 'Project Products',
		'singular_name'              => 'Project Products',
		'menu_name'                  => 'Project Products',
		'all_items'                  => 'All project products',
		'parent_item'                => 'Parent project product',
		'parent_item_colon'          => 'Parent project product:',
		'new_item_name'              => 'New Project Product Name',
		'add_new_item'               => 'Add New Product',
		'edit_item'                  => 'Edit Product',
		'update_item'                => 'Update Product',
		'view_item'                  => 'View Product',
		'separate_items_with_commas' => 'Separate products with commas',
		'add_or_remove_items'        => 'Add or remove products',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular Products',
		'search_items'               => 'Search Products',
		'not_found'                  => 'Not Found',
		'no_terms'                   => 'No products',
		'items_list'                 => 'Products list',
		'items_list_navigation'      => 'Products list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( PL_PROJECTS_PRODUCTS_TAX_NAME, PL_PROJECTS_CPT_NAME, $args );

}
add_action( 'init', 'pl_project_products_register_taxonomy', 0 );
